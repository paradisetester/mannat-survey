import { Component } from '@angular/core';
import { Nav,NavController } from 'ionic-angular';
import { data } from './home-data';
import { TelehealthPage } from '../telehealth/telehealth';
import { StatesPage } from '../states/states';

import { FourtybPage } from '../fourtyb/fourtyb';
import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { ProfilePage } from '../profile/profile';

import { RestProvider } from '../../services/rest';
import { SeoService } from '../../services/seo-service';

import { AuthService } from '../../services/auth.service';

import * as $ from "jquery";
declare var checkJsonsud: any;
declare var tabledatasud: any;
declare var tableIonic: any;
declare var USAmapLoad: any;
declare var tabledata: any;
declare var googleTagManager: any;
@Component({
	templateUrl: 'home.html',
	providers: [RestProvider]
})
export class HomePage {
	states : any;
	private nav: Nav;
    page_access : any;
    users : any;
	 rootPage;

	constructor(
	nav: Nav,
	public restProvider: RestProvider,
	private auth: AuthService,
	public navCtrl: NavController,
	seo: SeoService
	) {	
		this.nav = nav;
		this.getStates();
        
			
		seo.addTwitterCard(
		  'Substance Use Disorder',
		  'Substance Use Disorder',
		  'Medicaid Coverage Waivers: State Profiles',
		  '../../assets/images/logo.png'
		); 
		
		}
  ionViewCanEnter(): boolean | Promise<any> {
	  
	
	  
   return this.auth.accesspage('sud');
  } 

  
	ionViewDidLoad(){
		
	let TIME_IN_MS =500;
	  let hideFooterTimeout = setTimeout( () => {
  USAmapLoad('usaTerritories','sud','1');
	
	googleTagManager(this.auth.getUid());
	  }, TIME_IN_MS);
	 }

openPage(page) {
		this.nav.setRoot(page.component);
	}

	goToTelehealthPage(pageName){
		this.navCtrl.push(TelehealthPage);
	}
 

	getStates() {
		this.restProvider.getState()
		.then(data => {
			this.states = data;
			//console.log(this.states);
		});
	};


	logout() {
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}



}
