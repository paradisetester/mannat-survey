import { Component } from '@angular/core';
import { Nav, IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { LoginPage } from '../login/login';
import { RestProvider } from '../../services/rest';
import { AuthService } from '../../services/auth.service';
import { SeoService } from '../../services/seo-service';


 import * as $ from "jquery";
declare var checkJson340b: any;
declare var USAmapLoad: any;

@IonicPage()
@Component({
  selector: 'page-fourtyb',
  templateUrl: 'fourtyb.html',
})
export class FourtybPage {
	states : any;
  constructor(public nav: Nav, public navCtrl: NavController,
  public restProvider: RestProvider, public http: HttpModule,private auth: AuthService,
  
	seo: SeoService) {
	
		this.getStates();
		seo.addTwitterCard(
		  '340B 50 State Survey',
		  '340B 50 State Survey',
		  '340B 50 State Survey',
		  '../../assets/images/logo.png'
		); 
		
  }

  getStates() {
	this.restProvider.getState340b()
	.then(data => {
	  this.states = data;
	});
	};
	onSelectChange(selectedValue: any) {
		checkJson340b(selectedValue);
	}


ionViewCanEnter(): boolean | Promise<any> {
    return this.auth.accesspage('340b');
  } 

 

  ionViewDidLoad() {

		let TIME_IN_MS = 500;
		let hideFooterTimeout = setTimeout( () => {
			USAmapLoad('usaTerritories4','340b','4');
		}, TIME_IN_MS);

  }
logout() {
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}
}
