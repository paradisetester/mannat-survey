import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home.page';
import { ProfilePage } from '../profile/profile';
import { AuthService } from '../../services/auth.service';
import { SignupPage } from '../signup/signup';
import * as $ from "jquery";
import { Location } from '@angular/common';
import { SeoService } from '../../services/seo-service';


declare var getUrlParameter: any;

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	loginForm: FormGroup;
	loginError: string;
	credentials: string;

	constructor(
		private navCtrl: NavController,
		private auth: AuthService,	
		private _location: Location,
		fb: FormBuilder,
		seo: SeoService
	) {
	
		seo.addTwitterCard(
		  'Login',
		  'Login',
		  'Login',
		  '../../assets/images/logo.png'
		); 
		
		this.loginForm = fb.group({
			email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
		});
		
		
		
	}
		
	
	
	ionViewDidLoad(){		
		  let TIME_IN_MS = 500;
		  let hideFooterTimeout = setTimeout( () => {
	/* 	  var emails = getUrlParameter('email');
		  var password = getUrlParameter('token');
		 if(emails){		 	
				this.autoLogin(emails,password);
			 } */
		
	  }, TIME_IN_MS);  
	 }
autoLogin(email,password){
	let credentials = {
			email: email,
			password: password
		};
		this.auth.signInWithEmail(credentials)
			.then(
				() => this.navCtrl.setRoot(ProfilePage),
				error => this.loginError = error.message
			);
}

login() {
		let data = this.loginForm.value;

		if (!data.email) {
			return;
		}

		let credentials = {
			email: data.email,
			password: data.password
		};
		this.auth.signInWithEmail(credentials)
			.then(
				() => this.navCtrl.setRoot(ProfilePage),
				error => this.loginError = error.message
			);
	}
	
	
	loginWithGoogle() {
	  this.auth.signInWithGoogle()
		.then(
		  () => this.navCtrl.setRoot(HomePage),
		  error => console.log(error.message)
		);
	}
	signupbtn(){
	  this.navCtrl.push(SignupPage);
	}
}