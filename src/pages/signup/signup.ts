import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home.page';
import { AuthService } from '../../services/auth.service';
import { LoginPage } from '../login/login';
import { SeoService } from '../../services/seo-service';
import { Storage } from '@ionic/storage';
import { ProfilePage } from '../profile/profile';
@Component({
	selector: 'as-page-signup',
	templateUrl: './signup.html'
})
export class SignupPage {
	signupError: string;
	form: FormGroup;
	 authState: any = null;
  selectedArray :any = [];
  checkboxpages :any = [];
  datachecbox :any = [];
	constructor(
		fb: FormBuilder,
		private navCtrl: NavController,
		private auth: AuthService,
		private store : Storage,
		seo: SeoService
	) {
		seo.addTwitterCard(
		  'Sign Up',
		  'Sign Up',
		  'Sign Up',
		  '../../assets/images/logo.png'
		); 
		
		this.form = fb.group({
			email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
		});
		
		 this.checkboxpages = [
       {pageid: 1, name: "sud", checked: false},
       {pageid: 2, name: "telehealth", checked: false},
       {pageid: 3, name: "1115states", checked: false},
       {pageid: 4, name: "340b", checked: false}
    ];
	
	
  }
  selectpage(data){
 if (data.checked == true) {
    this.selectedArray.push(data);
   
  } else {
   let newArray = this.selectedArray.filter(function(el) {
     return el.pageid !== data.pageid;
  });
   this.selectedArray = newArray;
 }
 console.log(this.selectedArray);
}

  signup() {
	   this.signupError = "processing...";
			let data = this.form.value;
			
			this.selectedArray.forEach((page) => {
	
                  if(page.checked==true)
				  {
				 this.datachecbox.push(page.name);
				  }
           });
		   
		   
			let credentials = {
				email: data.email,
				password: data.password,
				pages : JSON.stringify(this.datachecbox)
			};
			 this.auth.signUp(credentials).then((user) =>  {
            this.authState = user			
			this.auth.updateUserData(this.authState.user.uid,credentials.pages);	
			 this.authState.user.sendEmailVerification();
			  this.store.set('seelcted_page', JSON.stringify(this.datachecbox));
			this.navCtrl.setRoot(ProfilePage);
             
      })
	 .catch(error => { this.signupError = error.message;})
		}
		
	loginbtn(){
	  this.navCtrl.push(LoginPage);
	}

}