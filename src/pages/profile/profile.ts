import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFirestore ,AngularFirestoreCollection} from 'angularfire2/firestore';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
interface Use { id: string; pages: string; }
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
signupError: string;
	profileForm: FormGroup;
	selectedArray :any = [];
	checkboxpages :any = [];
	datachecbox :any = [];
	users: Observable<Use[]>;
	userdata : any = [];
	userscollection: AngularFirestoreCollection;
	
  constructor(public navCtrl: NavController, 
  public navParams: NavParams,fb: FormBuilder,
  private auth: AuthService,
  public afAuth: AngularFireAuth, 
   private store : Storage,
  public firestore: AngularFirestore) {
	 
	  this.Getalluserdata();
	  this.profileForm = fb.group({
			fname: ['', Validators.compose([Validators.required,Validators.email])],
			lname: ['', Validators.compose([Validators.required])],
			email: ['', Validators.compose([Validators.required])],
			phone_number: ['', Validators.compose([Validators.required])],
			pages: ['', Validators.compose([Validators.required])],
		});
		 this.checkboxpages = [
       {pageid: 1, name: "sud", checked: false},
       {pageid: 2, name: "telehealth", checked: false},
       {pageid: 3, name: "1115states", checked: false},
       {pageid: 4, name: "340b", checked: false}
    ];
	
	
	  
  }



selectpage(data){
 if (data.checked == true) {
   // this.selectedArray.push(data);
   
  } else {
   let newArray = this.selectedArray.filter(function(el) {
     return el.pageid !== data.pageid;
  });
   //this.selectedArray = newArray;
 }
 //console.log(this.selectedArray);
}


Getalluserdata()
{ 
	 this.userscollection = this.firestore.collection<Use>('mannat');
			  this.userscollection.snapshotChanges().map(
				  changes => {
					return changes.map(
					  a => {
						const data = a.payload.doc.data() as Use;
						data.id = a.payload.doc.id;
						return data;
					  });
                       }).subscribe((Use) => {
										  Use.forEach(doc => {
											  
							if(doc.id==this.auth.getUid())
							{
								this.userdata = doc;
								
								  let userpaegsdata = JSON.parse(this.userdata.pages);
		 
		  this.checkboxpages = [
       {pageid: 1, name: "sud", checked: (userpaegsdata.indexOf('sud') > -1)},
       {pageid: 2, name: "telehealth", checked: (userpaegsdata.indexOf('telehealth') > -1)},
       {pageid: 3, name: "1115states", checked: (userpaegsdata.indexOf('1115states') > -1)},
       {pageid: 4, name: "340b", checked: (userpaegsdata.indexOf('340b') > -1)}
    ];
			this.selectedArray =  this.checkboxpages;					
							}
						 }) 
				          }
				   );
}


profile_form() {
		let data = this.profileForm.value;
this.selectedArray.forEach((page) => {
	
                  if(page.checked==true)
				  {
				 this.datachecbox.push(page.name);
				  }
           });

  
  
		if (!data.email) {
			return;
		}
		
		
 this.firestore.doc<any>('mannat/'+this.auth.getUid()).set({
			uid: this.auth.getUid(),
            fname: data.fname,
			pages: JSON.stringify(this.datachecbox),
            lname: data.lname,
            email: data.email,
            phoneumber: data.phone_number,
			});
			this.store.set('seelcted_page', JSON.stringify(this.datachecbox));
			this.datachecbox = [];
			
	}
	

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
