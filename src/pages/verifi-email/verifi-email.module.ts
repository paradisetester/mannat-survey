import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerifiEmailPage } from './verifi-email';

@NgModule({
  declarations: [
    VerifiEmailPage,
  ],
  imports: [
    IonicPageModule.forChild(VerifiEmailPage),
  ],
})
export class VerifiEmailPageModule {}
