import { Injectable  } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {AuthCredential} from '@firebase/auth-types';
import AuthProvider = firebase.auth.AuthProvider;
import { AngularFirestore ,AngularFirestoreCollection} from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
interface Use { id: string; pages: string; }
@Injectable()
export class AuthService {
	private user: firebase.User;
    authState: any = null;
   	users: Observable<Use[]>;
	userscollection: AngularFirestoreCollection;
	userIDresponse: any = null;
	  
	constructor(public afAuth: AngularFireAuth, private firestore: AngularFirestore, private store : Storage) {
		afAuth.authState.subscribe(user => {
			this.user = user;
			
		});
		 
			
	}

	signInWithEmail(credentials) {
		console.log('Sign in with email');
		return this.afAuth.auth.signInWithEmailAndPassword(credentials.email,
			 credentials.password).then((user) =>  {
            this.authState = user			
			 this.userscollection = this.firestore.collection<Use>('mannat');
			  this.userscollection.snapshotChanges().map(
				  changes => {
					return changes.map(
					  a => {
						const data = a.payload.doc.data() as Use;
						data.id = a.payload.doc.id;
						return data;
					  });
                       }).subscribe((Use) => {
										  Use.forEach(doc => {
											  
							if(doc.id==this.authState.user.uid)
							{
								 this.store.set('seelcted_page', doc.pages);
								
								
							}
						 }) 
				          }
				   );
             
      });
	}
	
	 accesspage(currentpage) {
		 
    return new Promise((resolve, reject) => {
      setTimeout(() => {
		  
		   this.store.get('seelcted_page').then((val) => {
           if(val)
		   {
		  resolve((JSON.parse(val).indexOf(currentpage) > -1));
		   }
		   else{
		   resolve(true);
		   }
         });
       
      },10);
    });
  }

  
	getUid() {
		  return this.user && this.user.uid;
		}
		
		signUp(credentials) {
		return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
    } 
	

updateUserData(uid,pages) {
		
		 this.firestore.doc<any>('mannat/'+uid).set({
			uid: uid,
            pages: pages
			});
	} 

	get authenticated(): boolean {
	  return this.user !== null;
	}
	
	 
	getEmail() {
		  return this.user && this.user.email;
		}
		
		
		
		
		
	signOut(): Promise<void> {
	  return this.afAuth.auth.signOut();
	}
	
	signInWithGoogle() {
			console.log('Sign in with google');
			return this.oauthSignIn(new firebase.auth.GoogleAuthProvider());
	}

	private oauthSignIn(provider: AuthProvider) {
		if (!(<any>window).cordova) {
			return this.afAuth.auth.signInWithPopup(provider);
		} 
}

}