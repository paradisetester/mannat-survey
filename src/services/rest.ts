import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
apiUrl = 'assets/js/';
  constructor(public http: HttpClient) {
    console.log('API enable');
	}
getState() {
  return new Promise(resolve => {
    this.http.get(this.apiUrl+'sudstates.json').subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}

getState1115() {
  return new Promise(resolve => {
    this.http.get(this.apiUrl+'1115states.json').subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}

getState340b() {
  return new Promise(resolve => {
    this.http.get(this.apiUrl+'340bstates.json').subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}


getStateTelehealth() {
  return new Promise(resolve => {
    this.http.get(this.apiUrl+'telehealthstates.json').subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}
getCategoryTelehealth() {
  return new Promise(resolve => {
    this.http.get(this.apiUrl+'category.json').subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}

}