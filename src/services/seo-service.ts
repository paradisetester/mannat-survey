import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable()
export class SeoService {
  constructor(private title: Title, private meta: Meta) {}

  addTwitterCard(title, keywords, description, img) {
    // Set HTML Document Title
	 
    this.title.setTitle(title);
  
    // Add Twitter Card Metatags
    this.meta.updateTag({ name: 'title', content: title });
    this.meta.updateTag({ name: 'keywords', content: keywords });
    this.meta.updateTag({ name: 'description', content: description });
    this.meta.updateTag({ name: 'image', content: img });
  }
 
}