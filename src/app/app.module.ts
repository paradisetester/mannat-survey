import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Config } from '../config';
import { HomePage } from '../pages/home/home.page';
import { TelehealthPage } from '../pages/telehealth/telehealth';
import { StatesPage } from '../pages/states/states';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { FourtybPage } from '../pages/fourtyb/fourtyb';
import { ProfilePage } from '../pages/profile/profile';
import { MyApp } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from '../config';
import { AuthService } from '../services/auth.service';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { RestProvider } from '../services/rest';
import { HttpClientModule } from '@angular/common/http';
import { LocationStrategy, PathLocationStrategy, APP_BASE_HREF } from '@angular/common';
import {MyMainHeaderComponent} from '../components/my-main-header/my-main-header';
import { SeoService } from '../services/seo-service';

import { AngularFirestore ,AngularFirestoreModule } from 'angularfire2/firestore';
import { IonicStorageModule } from '@ionic/storage';
@NgModule({
	declarations: [
		MyApp,
		HomePage,
		LoginPage,
		TelehealthPage,
		SignupPage,
		StatesPage,
		FourtybPage,
		ProfilePage,
		MyMainHeaderComponent
	],
	imports: [
		BrowserModule,
		HttpModule,
		HttpClientModule,
		IonicModule.forRoot(MyApp, {}, {
		 links: [
		  { component: HomePage, name: 'Home', segment: '' },
		  { component: TelehealthPage, name: 'Telehealth', segment: 'telehealth' },
		  { component: StatesPage, name: 'States', segment: '1115states' },
		  { component: FourtybPage, name: 'Fourty', segment: '340b' },
		  { component: LoginPage, name: 'Login', segment: 'login' },
		  { component: SignupPage, name: 'Signup', segment: 'signup' },
		  { component: ProfilePage, name: 'Profile', segment: 'profile' }
		]
	  }),
		AgmCoreModule.forRoot(),
		AngularFireModule.initializeApp(firebaseConfig.fire),
        AngularFirestoreModule,
         IonicStorageModule.forRoot(),
		NgxErrorsModule
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		LoginPage,
		TelehealthPage,
		HomePage,
		StatesPage,
		SignupPage,
		FourtybPage,
		ProfilePage
	],
	providers: [
		Config,
		StatusBar,
		AngularFireAuth,
		AuthService,
		RestProvider,
		SeoService,
		AngularFirestore,
		
	 {provide: LocationStrategy, useClass: PathLocationStrategy}
	]
})
export class AppModule {
}
