import { Component, ViewChild } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';

import { MenuController, Nav, Platform } from 'ionic-angular';
import { HomePage } from '../pages/home/home.page';
import { TelehealthPage } from '../pages/telehealth/telehealth';
import { StatesPage } from '../pages/states/states';
import { FourtybPage } from '../pages/fourtyb/fourtyb';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ProfilePage } from '../pages/profile/profile';
import { VerifiEmailPage } from '../pages/verifi-email/verifi-email';
import { AuthService } from '../services/auth.service';
import { Storage } from '@ionic/storage';
@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	pages;
  rootPage: any ;


	@ViewChild(Nav) nav: Nav;

	constructor(
		private platform: Platform,
		private menu: MenuController,
		private statusBar: StatusBar,
		public store : Storage,
		private auth: AuthService
	) {
		// set our app's pages
		
		this.store.get('seelcted_page').then((val) => {
		this.pages = [];
		  if(val)
		  {
		 if((JSON.parse(val).indexOf('sud') > -1))
		 {
			this.pages.push({title: 'Home', component: HomePage});
		 }
		 if((JSON.parse(val).indexOf('telehealth') > -1))
		 {
			this.pages.push({title: 'Telehealth', component: TelehealthPage});
		 }
		 if((JSON.parse(val).indexOf('1115states') > -1))
		 {
			this.pages.push({title: '1115 States', component: StatesPage});
		 }
		 if((JSON.parse(val).indexOf('340b') > -1))
		 {
			this.pages.push({title: '340B', component: FourtybPage});
		 }
		this.pages.push({title: 'profile', component: ProfilePage});
		  } 
		 });	
		
		
  
  
		   
this.initializeApp(this.pages);

		//this.rootPage = LoginPage;
	}

	initializeApp(page) {
		  this.platform.ready().then(() => {
			this.statusBar.styleDefault();
		  });


		 this.auth.afAuth.authState
			.subscribe(
			  user => {
				if(user){
					
					this.rootPage = page.component;
				     }
					
				 else {
				  this.rootPage = LoginPage;
				}
			  },
			  () => {
			    alert(123);
				this.rootPage = LoginPage;
			  }
			); 
		}


	login() {
		this.menu.close();
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}

	logout() {
		this.menu.close();
		this.auth.signOut();
		this.nav.setRoot(HomePage);
	}

	openPage(page) {
		this.menu.close();
		this.auth.afAuth.authState
			.subscribe(
			  user => {
				if (user) {
					this.nav.setRoot(page.component);
				} else {
				  this.rootPage = LoginPage;
				}
			  },
			  () => {
				this.rootPage = LoginPage;
			  }
			);

	}

}
