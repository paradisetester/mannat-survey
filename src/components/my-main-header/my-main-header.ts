import { Component, ViewChild ,Input} from '@angular/core';
import { MenuController, Nav, Platform, NavController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home.page';
import { TelehealthPage } from '../../pages/telehealth/telehealth';
import { StatesPage } from '../../pages/states/states';
import { FourtybPage } from '../../pages/fourtyb/fourtyb';
import { LoginPage } from '../../pages/login/login';
import { SignupPage } from '../../pages/signup/signup';
import { ProfilePage } from '../../pages/profile/profile';
import { RestProvider } from '../../services/rest';
import { AuthService } from '../../services/auth.service';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the MyMainHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'my-main-header',
  templateUrl: 'my-main-header.html'
})
export class MyMainHeaderComponent {
   pages;
   rootPage;
  title: any;

  @ViewChild(Nav) nav: Nav;
  @Input('pagetitle') textToUse;

  constructor(
			  public navCtrl: NavController,
			  private auth: AuthService,
			  private menu: MenuController,
			  public store : Storage
	) 
   { 
   
   
   	this.store.get('seelcted_page').then((val) => {
		this.pages = [];
		
		 if(val)
		   {
		 if((JSON.parse(val).indexOf('sud') > -1))
		 {
			this.pages.push({title: 'Home', component: HomePage});
		 }
		 if((JSON.parse(val).indexOf('telehealth') > -1))
		 {
			this.pages.push({title: 'Telehealth', component: TelehealthPage});
		 }
		 if((JSON.parse(val).indexOf('1115states') > -1))
		 {
			this.pages.push({title: '1115 States', component: StatesPage});
		 }
		 if((JSON.parse(val).indexOf('340b') > -1))
		 {
			this.pages.push({title: '340B', component: FourtybPage});
		 }
		this.pages.push({title: 'profile', component: ProfilePage});
		   }
		 });	
		
		
		

    this.title = false;
	}

  


   ngAfterViewInit()
   {
    
   this.title = this.textToUse;

   }

login() {
		this.menu.close();
		this.auth.signOut();
		this.navCtrl.setRoot(LoginPage);
	}

 logout() {
		this.menu.close();
		this.auth.signOut();
		this.navCtrl.setRoot(LoginPage);
	}
	 openPagemain(page) {
		this.menu.close();
		this.auth.afAuth.authState
			.subscribe(
			  user => {
				if (user) {
					this.navCtrl.setRoot(page.component);
				} else {
				  this.rootPage = LoginPage;
				}
			  },
			  () => {
				this.rootPage = LoginPage;
			  }
			);


	}



}
