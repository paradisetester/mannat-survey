
/*******************************/
/*******Google tag manager******/
/*******************************/

function googleTagManager(uid){
	gtag('set', {'user_id': uid});
}

/*******************************/
/*******USA MAP Load************/
/*******************************/



function USAmapLoad(mapFile,PageName,id){


	jQuery('#usaTerritories-map'+id).JSMaps({
			map: mapFile,
			mapWidth: 800,
			mapHeight: 600,
			stateClickAction : "none",
			onStateClick : function(res) {

			switch(PageName){
				case 'sud':
					/***this function is in sud/sud.js file**/
					checkJsonsud(res.name);
				break;
				case 'telehealth':
					/***this function is in telehealth/telehealth.js file**/
					checkJsonTele(res.abbreviation);
				break;
				case '340b':
					/***this function is in 340b/340b.js file**/
					checkJson340b(res.name);
				break;
				case '1115':
					checkJson1115(res.name,res.color);
				break;
			}
				var dataLayer = window.dataLayer = window.dataLayer || [];
				dataLayer.push({'event': PageName+'Map','MapId': 'usaTerritories-map'+id});
			} ,
			onStateOver : function(res) {

			}
		});
}


/********************************/
/*********ajax load *************/
/********************************/
jQuery(function(){

loadAjaxFunction('telehealthdata.json','telehealth');
loadAjaxFunction('medicaiddata.json','sud');
loadAjaxFunction('fifteenstatesdata.json','1115');
loadAjaxFunction('sectiondata.json','340b');

});


var suddata,telehealthdata,data340b,data1115;

function loadAjaxFunction(file,PageName){
    current = file;
    jQuery.ajax({
        url: BASE_URL+file,
        type: 'GET',
        dataType: "json",
        data: [],
        success: function (data, status, xhr) {

    if(data.length){
		switch(PageName){
				case 'sud':
					/***this function is in sud/sud.js file**/
					 suddata = data;
				break;
				case 'telehealth':
					/***this function is in telehealth/telehealth.js file**/
					 telehealthdata = data;
				break;
				case '340b':
					 data340b = data;
				break;
				case '1115':
					 data1115 = data;
				break;
			}
			}
}
	});
}
jQuery(document).on('change', '.select_state_sud', function (event) {
        var st = this.value;
       checkJsonsud(st);
    });

jQuery(document).on('change', '.select_state_fourth', function (event) {
        var sud = this.value;
       checkJson340b(sud);
    });
jQuery(document).on('change', '.select_state_1115states', function (event) {
        var states = this.value;
       checkJson1115(states);
    });
jQuery(document).on('change', '.select_state_telehealth', function (event) {

        var tele = this.value;
       checkJsonTele(tele);
    });

jQuery(document).on('change', '.select_cat_telehealth', function (event) {

        var cat = this.value;
       categoryChange(cat);
    });


/************************************/
/*********Click Function*************/
/************************************/
	function show_map(){
		$("#state_data").hide();
		$(".table_hidee").hide();
		$('.map_section').show();
	}


	function show_chart(id){

		$("#state_data").hide();
		$(".table_hidee").show();
		if(id==1)
		{
	//	tableIonic().setData(tabledatasud);
		}
		if(id==3)
		{
			table1115().setData(tabledata);
		}


		$('.map_section').hide();
	}

function mobile_responsive_table(){

	if(jQuery(window).width() < 768) {
jQuery('.table-responsive').each(function()
                                 {
var $this_table = jQuery(this);
var $html = '<div class="mobiel_issue_in_responsive">';
var html_tr = '';
$this_table.find('tr').each(function(){
 html_tr =html_tr+'<div class="table_item question_section">';
var $this_tr = jQuery(this);
var html_td = '';
$this_tr.find('td').each(function(index)
                             {
var $this_td = jQuery(this);
var $this_id_html = $this_td.html();
  if($this_id_html) {
 html_td =html_td+'<div class="tr_item"><span>'+$this_table.find('thead tr th').eq(index).html()+'</span><p>'+$this_id_html+'</p></div>';
  }
})
 html_tr =html_td+'</div>';

});
$html = $html+html_tr+'</div>';
$this_table.append($html);
$this_table.addClass('mobile_responsive_table');
});


var acc = document.getElementsByClassName("section-title");
var i;
for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {

    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}


	}
}


function hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',0.5)';
    }
    throw new Error('Bad Hex');
}
