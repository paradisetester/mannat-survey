//create Tabulator on DOM element with id "example-table"
function table1115() {
	var table = new Tabulator("#example-table-theme-semantic-ui", {
    columnVertAlign:"bottom", //align header contents to bottom of cel
	 tooltips:true,
	 tooltipsHeader:true,
    columns:[
	{title:"", field:"fname", width:150, headerSort:false, frozen:'true',},
    {title:"Approved Coverage Waivers",
        columns:[
        {title:"AZ", field:"AZ", align:"right", sorter:"number", width:60, headerSort:false, formatter:"image"},
        {title:"AR", field:"AR", align:"center", width:60, headerSort:false, formatter:"image"},
        {title:"IN", field:"IN", align:"center", width:60, headerSort:false, formatter:"image"},
		{title:"IA", field:"IA", align:"right", sorter:"number", width:60, headerSort:false, formatter:"image"},
        {title:"KY", field:"KY", align:"center", width:60, headerSort:false, formatter:"image"},
        {title:"MA", field:"MA", align:"center", width:70, headerSort:false, formatter:"image"},
		{title:"MI", field:"MI", align:"center", width:60, headerSort:false, formatter:"image"},
		{title:"MT", field:"MT", align:"center", width:60, headerSort:false, formatter:"image"},
		{title:"NH", field:"NH", align:"center", width:60, headerSort:false, formatter:"image"},
		{title:"UT", field:"UT", align:"center", width:60, headerSort:false, formatter:"image"},
		{title:"WI", field:"WI", align:"center", width:60, headerSort:false, formatter:"image"}
        ],
    },
    {
        title:"New or Amended Coverage Waiver Proposals ",
        columns:[
        {title:"AL", field:"AL", width:60, headerSort:false, formatter:"image"},
        {title:"AZ", field:"AZ1", width:60, headerSort:false, formatter:"image"},
		{title:"KS", field:"KS", width:60, headerSort:false, formatter:"image"},
		{title:"ME", field:"ME", width:60, headerSort:false, formatter:"image"},
		{title:"MA", field:"MA1", width:60, headerSort:false, formatter:"image"},
		{title:"MI", field:"MI1", width:60, headerSort:false, formatter:"image"},
		{title:"MS", field:"MS", width:60, headerSort:false, formatter:"image"},
		{title:"NH", field:"NH", width:60, headerSort:false, formatter:"image"},
        {title:"NM", field:"NM", width:60, headerSort:false, formatter:"image"},
		{title:"NC", field:"NC", width:60, headerSort:false, formatter:"image"},
		{title:"OH", field:"OH", width:60, headerSort:false, formatter:"image"},
		{title:"SD", field:"SD", width:60, headerSort:false, formatter:"image"},
		{title:"UT1", field:"UT1", width:60, headerSort:false, formatter:"image"},
		{title:"UT2", field:"UT2", width:60, headerSort:false, formatter:"image"},
		{title:"WI", field:"WI1", width:60, headerSort:false, formatter:"image"}
        ],
    },
    ],
});
return table;
}
var tabledata = [
	{id:1, fname:"Premiums", AZ:"blue.png", AR:"blue.png", IN:"blue.png", IA:"blue.png", KY:"blue.png", MA:"green.png", MI:"blue.png", MT:"blue.png", NH:"", UT:"", WI:"blue.png", AL:"", AZ1:"blue.png", KS:"", ME:"", MI1:"", MA1:"", MS:"", NH:"", NM:"", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:""},

	{id:2, fname:"Cost Sharing", AZ:"red.png", AR:"red.png", IN:"red.png", IA:"red.png", KY:"red.png", MA:"red.png", MI:"red.png", MT:"red.png", NH:"red.png", UT:"red.png", WI:"red.png", AL:"", AZ1:"red.png", KS:"", ME:"green.png", MI1:"red.png", MA1:"red.png", MS:"", NH:"red.png", NM:"", NC:"", OH:"", SD:"", UT1:"green.png", UT2:"", WI1:"green.png"},

	{id:3, fname:"Cost Sharing Tracking Waiver", AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"green.png", MS:"", NH:"green.png", NM:"", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:""},

	{id:4, fname:"Work-Related Provisions", AZ:"red.png", AR:"blue.png", IN:"blue.png", IA:"", KY:"", MA:"", MI:"", MT:"red.png", NH:"blue.png", UT:"", WI:"", AL:"green.png", AZ1:"green.png", KS:"green.png", ME:"green.png", MI1:"", MA1:"", MS:"green.png", NH:"purple.png", NM:"", NC:"green.png", OH:"green.png", SD:"green.png", UT1:"green.png", UT2:"green.png", WI1:"green.png"},

	{id:5, fname:"Healthy Behavior Incentives",  AZ:"red.png", AR:"red.png", IN:"red.png", IA:"red.png", KY:"red.png", MA:"", MI:"", MT:"red.png", NH:"red.png", UT:"", WI:"", AL:"", AZ1:"red.png", KS:"", ME:"", MI1:"red.png", MA1:"green.png", MS:"", NH:"red.png", NM:"green.png", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:"red.png"},

	{id:6, fname:"NEMT Waiver",  AZ:"", AR:"", IN:"blue.png", IA:"blue.png", KY:"blue.png", MA:"", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"green.png", MI1:"", MA1:"", MS:"", NH:"green.png", NM:"", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:""},

	{id:7, fname:"EPSDT Waiver",  AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"", NH:"", UT:"blue.png", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"green.png", MS:"", NH:"", NM:"green.png", NC:"", OH:"", SD:"", UT1:"blue.png", UT2:"green.png", WI1:""},

	{id:8, fname:"Other Benefit-Related Provisions (e.g., Requiring Parents/ Caretakersto Receive ABP, Limited Benefit Packages for Certain Populations)",  AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"", NH:"", UT:"blue.png", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"green.png", MS:"", NH:"", NM:"green.png", NC:"", OH:"", SD:"", UT1:"", UT2:"",WI1:""},

	{id:9, fname:"IMD Exclusion Waiver3",  AZ:"", AR:"", IN:"blue.png", IA:"", KY:"blue.png", MA:"blue.png", MI:"", MT:"", NH:"", UT:"blue.png", WI:"", AL:"", AZ1:"green.png", KS:"green.png", ME:"", MI1:"green.png", MA1:"green.png", MS:"", NH:"green.png", NM:"green.png", NC:"green.png", OH:"", SD:"", UT1:"blue.png", UT2:"blue.png", WI1:"green.png"},

	{id:10, fname:"Retroactive Eligibility Waiver",  AZ:"", AR:"blue.png", IN:"blue.png", IA:"blue.png", KY:"blue.png", MA:"", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"green.png", MS:"", NH:"green.png", NM:"", NC:"", OH:"", SD:"", UT1:"blue.png", UT2:"", WI1:""},

	{id:11, fname:"Prompt Enrollment Waiver",  AZ:"", AR:"", IN:"blue.png", IA:"", KY:"blue.png", MA:"blue.png", MI:"", MT:"", NH:"blue.png", UT:"blue.png", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"", MS:"", NH:"green.png", NM:"green.png", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:""},

	{id:12, fname:"Elimination of Presumptive Eligibility",  AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"", MS:"", NH:"", NM:"green.png", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:""},

	{id:13, fname:"Asset Test", AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"green.png", MI1:"", MA1:"", MS:"", NH:"green.png", NM:"", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:""},

	{id:14, fname:"Drug Testing",  AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"", MS:"", NH:"", NM:"", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:"green.png"},

	{id:15, fname:"Limits on Enrollment Duration",  AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"green.png", KS:"", ME:"", MI1:"", MA1:"", MS:"", NH:"", NM:"", NC:"", OH:"", SD:"", UT1:"green.png", UT2:"", WI1:"green.png"},

	{id:16, fname:"Partial Expansion", AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"green.png", MS:"", NH:"", NM:"", NC:"", OH:"", SD:"", UT1:"", UT2:"green.png", WI1:""},

	{id:17, fname:"12-Month Continuous Eligibility",  AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"blue.png", NH:"", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"", MS:"", NH:"", NM:"", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:""},

	{id:18, fname:"Other Eligibility-Related Provisions (e.g., Modified Open Enrollment Periods, Changes to Redeterminations)",  AZ:"", AR:"", IN:"blue.png", IA:"", KY:"blue.png", MA:"blue.png", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"green.png", KS:"", ME:"", MI1:"", MA1:"green.png", MS:"", NH:"green.png", NM:"green.png", NC:"", OH:"", SD:"", UT1:"blue.png", UT2:"", WI1:""},

	{id:19, fname:"Section 1927 Waiver", AZ:"", AR:"", IN:"", IA:"", KY:"", MA:"", MI:"", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"green.png", MS:"", NH:"", NM:"", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:""},

	{id:20, fname:"Health Savings-Like Accounts", AZ:"red.png", AR:"", IN:"red.png", IA:"", KY:"red.png", MA:"", MI:"red.png", MT:"", NH:"", UT:"", WI:"", AL:"", AZ1:"red.png", KS:"red.png", ME:"", MI1:"red.png", MA1:"", MS:"", NH:"", NM:"", NC:"", OH:"", SD:"", UT1:"", UT2:"", WI1:""},

	{id:21, fname:"QHP Premium Assistance",  AZ:"", AR:"blue.png", IN:"", IA:"", KY:"", MA:"", MI:"blue.png", MT:"", NH:"blue.png", UT:"", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"purple.png", MA1:"", MS:"", NH:"", NM:"", NC:"", OH:"", SD:"green.png", UT1:"", UT2:"", WI1:""},

	{id:22, fname:"ESI Premium Assistance/HIPP", AZ:"", AR:"", IN:"", IA:"red.png", KY:"blue.png", MA:"red.png", MI:"", MT:"", NH:"red.png", UT:"blue.png", WI:"", AL:"", AZ1:"", KS:"", ME:"", MI1:"", MA1:"green.png", MS:"", NH:"red.png", NM:"", NC:"", OH:"", SD:"green.png", UT1:"blue.png", UT2:"blue.png", WI1:""},

];

function checktabledata(tabledata){
    jQuery.each( tabledata, function( key, value ) {
           if(value.AZ == 'blue.png'){
		     value.AZ="= Waiver obtained ";
		   }
		    if(value.AZ == 'red.png'){
		      value.AZ="= No waiver required";
		   }
		    if(value.AZ == 'green.png'){
		      value.AZ="= Waiver requested";
		   }
		   if(value.AZ == 'purple.png'){
		      value.AZ="= Waiver continuation requested";
		   }

		    if(value.AR == 'blue.png'){
		     value.AR="= Waiver obtained ";
		   }
		    if(value.AR == 'red.png'){
		      value.AR="= No waiver required";
		   }
		    if(value.AR == 'green.png'){
		      value.AR="= Waiver requested";
		   }
		   if(value.AR == 'purple.png'){
		      value.AR="= Waiver continuation requested";
		   }

		    if(value.IN == 'blue.png'){
		     value.IN="= Waiver obtained ";
		   }
		    if(value.IN == 'red.png'){
		      value.IN="= No waiver required";
		   }
		    if(value.IN == 'green.png'){
		      value.IN="= Waiver requested";
		   }
		   if(value.IN == 'purple.png'){
		      value.IN="= Waiver continuation requested";
		   }

		    if(value.IA == 'blue.png'){
		     value.IA="= Waiver obtained ";
		   }
		    if(value.IA == 'red.png'){
		      value.IA="= No waiver required";
		   }
		    if(value.IA == 'green.png'){
		      value.IA="= Waiver requested";
		   }
		   if(value.IA == 'purple.png'){
		      value.IA="= Waiver continuation requested";
		   }


		    if(value.KY == 'blue.png'){
		     value.KY="= Waiver obtained ";
		   }
		    if(value.KY == 'red.png'){
		      value.KY="= No waiver required";
		   }
		    if(value.KY == 'green.png'){
		      value.KY="= Waiver requested";
		   }
		   if(value.KY == 'purple.png'){
		      value.KY="= Waiver continuation requested";
		   }

		   if(value.MA == 'blue.png'){
		     value.MA="= Waiver obtained ";
		   }
		    if(value.MA == 'red.png'){
		      value.MA="= No waiver required";
		   }
		    if(value.MA == 'green.png'){
		      value.MA="= Waiver requested";
		   }
		   if(value.MA == 'purple.png'){
		      value.MA="= Waiver continuation requested";
		   }


		   if(value.MI == 'blue.png'){
		     value.MI="= Waiver obtained ";
		   }
		    if(value.MI == 'red.png'){
		      value.MI="= No waiver required";
		   }
		    if(value.MI == 'green.png'){
		      value.MI="= Waiver requested";
		   }
		   if(value.MI == 'purple.png'){
		      value.MI="= Waiver continuation requested";
		   }

		   if(value.MT == 'blue.png'){
		     value.MT="= Waiver obtained ";
		   }
		    if(value.MT == 'red.png'){
		      value.MT="= No waiver required";
		   }
		    if(value.MT == 'green.png'){
		      value.MT="= Waiver requested";
		   }
		   if(value.MT == 'purple.png'){
		      value.MT="= Waiver continuation requested";
		   }

		    if(value.NH == 'blue.png'){
		     value.NH="= Waiver obtained ";
		   }
		    if(value.NH == 'red.png'){
		      value.NH="= No waiver required";
		   }
		    if(value.NH == 'green.png'){
		      value.NH="= Waiver requested";
		   }
		   if(value.NH == 'purple.png'){
		      value.NH="= Waiver continuation requested";
		   }

		   if(value.NM == 'blue.png'){
		     value.NM="= Waiver obtained ";
		   }
		    if(value.NM == 'red.png'){
		      value.NM="= No waiver required";
		   }
		    if(value.NM == 'green.png'){
		      value.NM="= Waiver requested";
		   }
		   if(value.NM == 'purple.png'){
		      value.NM="= Waiver continuation requested";
		   }

		    if(value.UT == 'blue.png'){
		     value.UT="= Waiver obtained ";
		   }
		    if(value.UT == 'red.png'){
		      value.UT="= No waiver required";
		   }
		    if(value.UT == 'green.png'){
		      value.UT="= Waiver requested";
		   }
		   if(value.UT == 'purple.png'){
		      value.UT="= Waiver continuation requested";
		   }

		      if(value.WI == 'blue.png'){
		     value.WI="= Waiver obtained ";
		   }
		    if(value.WI == 'red.png'){
		      value.WI="= No waiver required";
		   }
		    if(value.WI == 'green.png'){
		      value.WI="= Waiver requested";
		   }
		   if(value.WI == 'purple.png'){
		      value.WI="= Waiver continuation requested";
		   }

		   if(value.AL == 'blue.png'){
		     value.AL="= Waiver obtained ";
		   }
		    if(value.AL == 'red.png'){
		      value.AL="= No waiver required";
		   }
		    if(value.AL == 'green.png'){
		      value.AL="= Waiver requested";
		   }
		   if(value.AL == 'purple.png'){
		      value.AL="= Waiver continuation requested";
		   }

		     if(value.AZ1 == 'blue.png'){
		     value.AZ1="= Waiver obtained ";
		   }
		    if(value.AZ1 == 'red.png'){
		      value.AZ1="= No waiver required";
		   }
		    if(value.AZ1 == 'green.png'){
		      value.AZ1="= Waiver requested";
		   }
		   if(value.AZ1 == 'purple.png'){
		      value.AZ1="= Waiver continuation requested";
		   }

		   if(value.KS == 'blue.png'){
		     value.KS="= Waiver obtained ";
		   }
		    if(value.KS == 'red.png'){
		      value.KS="= No waiver required";
		   }
		    if(value.KS == 'green.png'){
		      value.KS="= Waiver requested";
		   }
		   if(value.KS == 'purple.png'){
		      value.KS="= Waiver continuation requested";
		   }

		   if(value.ME == 'blue.png'){
		     value.ME="= Waiver obtained ";
		   }
		    if(value.ME == 'red.png'){
		      value.ME="= No waiver required";
		   }
		    if(value.ME == 'green.png'){
		      value.ME="= Waiver requested";
		   }
		   if(value.ME == 'purple.png'){
		      value.ME="= Waiver continuation requested";
		   }

		    if(value.MI1 == 'blue.png'){
		     value.MI1="= Waiver obtained ";
		   }
		    if(value.MI1 == 'red.png'){
		      value.MI1="= No waiver required";
		   }
		    if(value.MI1 == 'green.png'){
		      value.MI1="= Waiver requested";
		   }
		   if(value.MI1 == 'purple.png'){
		      value.MI1="= Waiver continuation requested";
		   }

		     if(value.MA1 == 'blue.png'){
		     value.MA1="= Waiver obtained ";
		   }
		    if(value.MA1 == 'red.png'){
		      value.MA1="= No waiver required";
		   }
		    if(value.MA1 == 'green.png'){
		      value.MA1="= Waiver requested";
		   }
		   if(value.MA1 == 'purple.png'){
		      value.MA1="= Waiver continuation requested";
		   }

		   if(value.MS == 'blue.png'){
		     value.MS="= Waiver obtained ";
		   }
		    if(value.MS == 'red.png'){
		      value.MS="= No waiver required";
		   }
		    if(value.MS == 'green.png'){
		      value.MS="= Waiver requested";
		   }
		   if(value.MS == 'purple.png'){
		      value.MS="= Waiver continuation requested";
		   }

		    if(value.NH == 'blue.png'){
		     value.NH="= Waiver obtained ";
		   }
		    if(value.NH == 'red.png'){
		      value.NH="= No waiver required";
		   }
		    if(value.NH == 'green.png'){
		      value.NH="= Waiver requested";
		   }
		   if(value.NH == 'purple.png'){
		      value.NH="= Waiver continuation requested";
		   }

		    if(value.NM == 'blue.png'){
		     value.NM="= Waiver obtained ";
		   }
		    if(value.NM == 'red.png'){
		      value.NM="= No waiver required";
		   }
		    if(value.NM == 'green.png'){
		      value.NM="= Waiver requested";
		   }
		   if(value.NM == 'purple.png'){
		      value.NM="= Waiver continuation requested";
		   }


		    if(value.NC == 'blue.png'){
		     value.NC="= Waiver obtained ";
		   }
		    if(value.NC == 'red.png'){
		      value.NC="= No waiver required";
		   }
		    if(value.NC == 'green.png'){
		      value.NC="= Waiver requested";
		   }
		   if(value.NC == 'purple.png'){
		      value.NC="= Waiver continuation requested";
		   }


		    if(value.OH == 'blue.png'){
		     value.OH="= Waiver obtained ";
		   }
		    if(value.OH == 'red.png'){
		      value.OH="= No waiver required";
		   }
		    if(value.OH == 'green.png'){
		      value.OH="= Waiver requested";
		   }
		   if(value.OH == 'purple.png'){
		      value.OH="= Waiver continuation requested";
		   }

		     if(value.SD == 'blue.png'){
		     value.SD="= Waiver obtained ";
		   }
		    if(value.SD == 'red.png'){
		      value.SD="= No waiver required";
		   }
		    if(value.SD == 'green.png'){
		      value.SD="= Waiver requested";
		   }
		   if(value.SD == 'purple.png'){
		      value.SD="= Waiver continuation requested";
		   }


		   if(value.UT1 == 'blue.png'){
		     value.UT1="= Waiver obtained ";
		   }
		    if(value.UT1 == 'red.png'){
		      value.UT1="= No waiver required";
		   }
		    if(value.UT1 == 'green.png'){
		      value.UT1="= Waiver requested";
		   }
		   if(value.UT1 == 'purple.png'){
		      value.UT1="= Waiver continuation requested";
		   }

		     if(value.UT2 == 'blue.png'){
		     value.UT2="= Waiver obtained ";
		   }
		    if(value.UT2 == 'red.png'){
		      value.UT2="= No waiver required";
		   }
		    if(value.UT2 == 'green.png'){
		      value.UT2="= Waiver requested";
		   }
		   if(value.UT2 == 'purple.png'){
		      value.UT2="= Waiver continuation requested";
		   }

		      if(value.WI1 == 'blue.png'){
		     value.WI1="= Waiver obtained ";
		   }
		    if(value.WI1 == 'red.png'){
		      value.WI1="= No waiver required";
		   }
		    if(value.WI1 == 'green.png'){
		      value.WI1="= Waiver requested";
		   }
		   if(value.WI1 == 'purple.png'){
		      value.WI1="= Waiver continuation requested";
		   }
    });
}

$("#download-xlsx").click(function(){
    checktabledata(tabledata);
    table.download("xlsx", "data.xlsx", {sheetName:"1115 chart"});
});



   jQuery(".my_togglee").click(function(){
		$('#usaTerritories-map3').hide();
		$(".table_hide").hide();
		jQuery(".chart_outr").show("main");
		$("#state_data").hide();

    });




function checkJson1115(abb,color){
      var thisData='';
    jQuery.each( data1115, function( key, value ) {
        if(value.state_name===abb){
            thisData = value;
		}
	});

    jQuery('#state_data').html('');
    if(!thisData){
        return;
	}
    createData(thisData,color);
	sud_map_load_click_on_State();
	 $("#state_data").show();
    jQuery(".table_hidee").hide();
    jQuery(".map_section").hide();
    jQuery(".chart_outrss").hide();

	$("html, body").animate({ scrollTop: 0 }, "slow");

    jQuery(this).prop('selected', false);
    jQuery("#states option").each(function( index ) {
        var chk = jQuery(this).val();
        if(chk==abb){
            jQuery(this).prop('selected', true);
		}
	});

}


function createData(arr,color) {
    var html='';
    var i=0;
    var source_arr = [];
    var source_link_arr = [];
    var $si = 1;
	var m = 1;
	var thback = '';
    var stehead = "";
    var trback = "";
	var title = arr.title;
    if(title.match(/Waiver Request/g)){
		html += ' <style type="text/css" rel="stylesheet"> .table100.ver1 .table100-body tr:nth-child(even) { background-color: #e0516c2e; !important;  } .table100.ver1 th { background-color: #e0516c !important; } .state-head h5 { color: #e0516c;} </style>';
		thback = 'background-color: #e0516c; !important;';
		stehead = "color: #e0516c; !important";
		trback = "background-color: #e0516c2e; !important;";
		//jQuery(".table100.ver1 .table100-body tr:nth-child(even)").css("");
	}

    if(title.match(/Approved Through/g)){
		thback = 'background-color: #1f6ccc; !important;';
		stehead = "color:#1f6ccc; !important";
		trback = "background-color: #1f6ccc26; !important;";
		//jQuery(".table100.ver1 .table100-body tr:nth-child(even)").css("");
	}
  if(color)
	{
		thback = 'background-color: '+color+' !important;';
			stehead = 'color: '+color+' !important';
				trback = "background-color: "+hexToRgbA(color)+" !important;";
}

    html +='\
    <div class="chart_outr content_outr">\
	<div class="limiter">\
	<div class="container-table100">\
	<div class="state-head">\
	<div class="state_headings" style="border-color: '+arr.color+';">\
	<h3 style="'+stehead+'">'+arr.state_name+'</h3>\
	<h5>'+arr.title+'</h5>';
	if(arr.description){
		html +='<h6>'+arr.description+'</h6>';
	}
	html +='</div>\
    <div class="wrap-table100"> <div class="table100 ver1 m-b-110"> <div class="table100-head"><table>';
	html += '<tr class="row100 head" ><th class="cell100 column" style="'+thback+'"></th><th class="cell100 column2" style="'+thback+'">'+ arr.first_heading+'</th>';
	if(arr.second_heading != '' && arr.second_heading != null ){
		html += '<th class="cell100 column2" style="'+thback+'">'+ arr.second_heading+'</th>'
	}
	html+='</tr></table></div><div class="table100-body js-pscroll"><table><tbody>';
	jQuery.each( arr.faqs, function( key, value ) {
		var str = value.description;

		var string =value.description;
		string = abbrivationstring(string);
		//var new_string = string.replace('&#8226;','<br/>&#8226;');
		//var new_string = string.replace(/•/g,'<br/>•');

		if(m % 2 == 0){
			html +='<tr class="row100 body" style="'+trback+'">';
			}else{
			html +='<tr class="row100 body">';
		}
		html +='<td class="cell100 column1" >'+value.category_name+'</td>\
		<td class="cell100 column2" >'+string+'\
		';

		html +='</td>';
		if(arr.second_heading !='' && arr.second_heading != null){
			if(value.description2 != '' && value.description2 != null){
				var string2 =value.description2;
				// var new_string2 = string2.replace(/•/g,'<br/>•');
				string2 = abbrivationstring(string);
				html +='<td class="cell100 column2">'+string2+'</td>';

				}else{
				html +='<td></td>';
			}
		}
		html +='</tr>';
		m++;
	});
	html +='</tbody></table></div>\
	</div>\
	</div>\
	</div>\
	</div>\
	';


    jQuery('#state_data').html(html);
}


function abbrivationstring(string){
    if(string.match(/SUD/g)){
        string = string.replace(/SUD/g,'<span class="tooltip1" title="Substance use disorder">SUD</span>');
	}
    if(string.match(/ABD/g)){
        string = string.replace(/ABD/g,'<span class="tooltip1" title="Aged, blind, and disabled">ABD</span>');
	}

    if(string.match(/MCO/g)){
        string = string.replace(/MCO/g,'<span class="tooltip1" title="Managed care organization">MCO</span>');
	}

    if(string.match(/ACA/g)){
        string = string.replace(/ACA/g,'<span class="tooltip1" title="Affordable Care Act">ACA</span>');
	}

    if(string.match(/MH/g)){
        string = string.replace(/MH/g,'<span class="tooltip1" title="Mental health">MH</span>');
	}

    if(string.match(/ACO/g)){
        string = string.replace(/ACO/g,'<span class="tooltip1" title="Accountable care organization">ACO</span>');
	}

    if(string.match(/MLTSS/g)){
        string = string.replace(/MLTSS/g,'<span class="tooltip1" title="Managed long-term services and supports">MLTSS</span>');
	}

    if(string.match(/ACT/g)){
        string = string.replace(/ACT/g,'<span class="tooltip1" title="Assertive community treatment">ACT</span>');
	}

    if(string.match(/MMC/g)){
        string = string.replace(/MMC/g,'<span class="tooltip1" title="Medicaid managed care">MMC</span>');
	}

    if(string.match(/NEMT/g)){
        string = string.replace(/NEMT/g,'<span class="tooltip1" title="Non-emergency medical transportation">NEMT</span>');
	}

    if(string.match(/ASAM/g)){
        string = string.replace(/ASAM/g,'<span class="tooltip1" title="American Society of Addiction Medicine">ASAM</span>');
	}
    if(string.match(/PACE/g)){
        string = string.replace(/PACE/g,'<span class="tooltip1" title="Programs of All-Inclusive Care for the Elderly">PACE</span>');
	}

    if(string.match(/ASO/g)){
        string = string.replace(/ASO/g,'<span class="tooltip1" title="Administrative services organization">ASO</span>');
	}

	if(string.match(/PCCM/g)){
        string = string.replace(/PCCM/g,'<span class="tooltip1" title="Primary care case management">PCCM</span>');
	}

    if(string.match(/BH/g)){
        string = string.replace(/BH/g,'<span class="tooltip1" title="Behavioral health">BH</span>');
	}

    if(string.match(/PIHP/g)){
        string = string.replace(/PIHP/g,'<span class="tooltip1" title="Prepaid inpatient health plan">PIHP</span>');
	}

    if(string.match(/CBO/g)){
        string = string.replace(/CBO/g,'<span class="tooltip1" title="Community-based organization">CBO</span>');
	}

    if(string.match(/PRTF/g)){
        string = string.replace(/PRTF/g,'<span class="tooltip1" title="Psychiatric residential treatment facility">PRTF</span>');
	}

    if(string.match(/CHIP/g)){
        string = string.replace(/CHIP/g,'<span class="tooltip1" title="Children’s Health Insurance Program">CHIP</span>');
	}

    if(string.match(/QHP/g)){
        string = string.replace(/QHP/g,'<span class="tooltip1" title="Qualified health plan">QHP</span>');
	}

	if(string.match(/CMS/g)){
        string = string.replace(/CMS/g,'<span class="tooltip1" title="Centers for Medicare & Medicaid Services">CMS</span>');
	}

    if(string.match(/RFP/g)){
        string = string.replace(/RFP/g,'<span class="tooltip1" title="Request for proposals">RFP</span>');
	}

    if(string.match(/DSHP/g)){
        string = string.replace(/DSHP/g,'<span class="tooltip1" title="Designated state health program">DSHP</span>');
	}

    if(string.match(/SBIRT/g)){
        string = string.replace(/SBIRT/g,'<span class="tooltip1" title="Screening, brief intervention, and referral to treatment">SBIRT</span>');
	}

    if(string.match(/DSRIP/g)){
        string = string.replace(/DSRIP/g,'<span class="tooltip1" title="Delivery System Reform and Incentive">DSRIP</span>');
	}
    if(string.match(/SED/g)){
        string = string.replace(/SED/g,'<span class="tooltip1" title="Serious emotional disturbance">SED</span>');
	}

    if(string.match(/SMD/g)){
        string = string.replace(/SED/g,'<span class="tooltip1" title="State Medicaid director">SMD</span>');
	}

    if(string.match(/ED/g)){
        string = string.replace(/ED/g,'<span class="tooltip1" title="Emergency department">ED</span>');
	}

	if(string.match(/FFS/g)){
        string = string.replace(/FFS/g,'<span class="tooltip1" title="Fee-for-service">FFS</span>');
	}

	if(string.match(/SMI/g)){
        string = string.replace(/SMI/g,'<span class="tooltip1" title="Serious mental illness">SMI</span>');
	}

    if(string.match(/FPL/g)){
        string = string.replace(/FPL/g,'<span class="tooltip1" title="Federal poverty level">FPL</span>');
	}
    if(string.match(/SPMI/g)){
        string = string.replace(/SPMI/g,'<span class="tooltip1" title="Severe and persistent mental illness">SPMI</span>');
	}

    if(string.match(/HCBS/g)){
        string = string.replace(/HCBS/g,'<span class="tooltip1" title="Home- and community-based services">HCBS</span>');
	}

    if(string.match(/SSI/g)){
        string = string.replace(/SSI/g,'<span class="tooltip1" title="Supplemental Security Income">SSI</span>');
	}
    if(string.match(/TBI/g)){
        string = string.replace(/TBI/g,'<span class="tooltip1" title="Traumatic brain injury">TBI</span>');
	}
    if(string.match(/TMA/g)){
        string = string.replace(/TMA/g,'<span class="tooltip1" title="Transitional Medical Assistance">TMA</span>');
	}

    if(string.match(/IMD/g)){
        string = string.replace(/IMD/g,'<span class="tooltip1" title="Institutions for mental disease">IMD</span>');
	}

    if(string.match(/MAT/g)){
        string = string.replace(/MAT/g,'<span class="tooltip1" title="Medication-assisted treatment">MAT</span>');
	}

    if(string.match(/WM/g)){
        string = string.replace(/WM/g,'<span class="tooltip1" title="Withdrawal management">WM</span>');
	}

    if(string.match(/WM/g)){
        string = string.replace(/WM/g,'<span class="tooltip1" title="Withdrawal management">WM</span>');
	}


    return string;

}

jQuery(document).ready(function($){
	$('body').tooltip({
		selector: '.tooltip1'
	});
});

	function sud_map_load_click_on_State() {

	if(jQuery(window).width() < 768) {
   jQuery('.wrap-table100').each(function() {
   var $this = $(this);
   var $this_table = jQuery(this).find('.table100-body table');
   var $this_head = jQuery(this).find('.table100-head table');
   var $html = '<div class="custom_Table">';
   var html_tr = '';
   $this_table.find('tr').each(function() {

    var $this_tr = jQuery(this);
     html_tr = html_tr + '<div class="row_data"><div class="heading heading_sud">'+$this_tr.find('td').eq(0).html()+'</div><div class="td_contents">';
    var html_td = '';
    $this_tr.find('td').each(function(index) {
     var $this_td = jQuery(this);
     var $this_id_html = $this_td.html();
     if ($this_id_html && (index!=0)) {
      html_td = html_td + '<div class="td_content"><div class="td_heading">' + $this_head.find('tbody tr th').eq(index).html() + '</div><div class="td_content">' + $this_id_html + '</div></div>';
     }
    });
	html_tr = html_tr + html_td + '</div></div>';
   });
   $html = $html + html_tr + '</div>';
   $this.append($html);
   $this.addClass('mobile_custom_responsive_table');
  });


  var acc = document.getElementsByClassName("heading_sud");

var i;
for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {

    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}


	}

	}
