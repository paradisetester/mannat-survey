webpackJsonp([0],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestProvider = /** @class */ (function () {
    function RestProvider(http) {
        this.http = http;
        this.apiUrl = 'assets/js/';
        console.log('API enable');
    }
    RestProvider.prototype.getState = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'sudstates.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getStateTelehealth = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'telehealthstates.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getCategoryTelehealth = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'category.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TelehealthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TelehealthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TelehealthPage = /** @class */ (function () {
    function TelehealthPage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.getStates();
        this.getCategory();
    }
    TelehealthPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TelehealthPage');
    };
    TelehealthPage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getStateTelehealth()
            .then(function (data) {
            _this.states = data;
            console.log(_this.states);
        });
    };
    ;
    TelehealthPage.prototype.getCategory = function () {
        var _this = this;
        this.restProvider.getCategoryTelehealth()
            .then(function (data) {
            _this.category = data;
            console.log(_this.category);
        });
    };
    ;
    TelehealthPage.prototype.onSelectChange = function (selectedValue) {
        //stateChange(selectedValue); 
    };
    TelehealthPage.prototype.onCategoryChange = function (selectedValue) {
        //categoryChange(selectedValue); 
    };
    TelehealthPage.prototype.goToHomePage = function (pageName) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    TelehealthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-telehealth',template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\state\state-survay\src\pages\telehealth\telehealth.html"*/'<ion-content> \n<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<div class="logo">\n			<img src="../../assets/images/logo.jpg">\n		</div>\n	</ion-navbar>\n</ion-header>\n<ion-content> \n	<!---header--->\n	\n	\n	\n	\n	<header>\n		<div class="header_section">\n			<div class="container">\n				<div class="logo">\n					<img src="../../assets/images/logo.jpg">\n				</div>\n				\n				<div class="nav">\n					<ul>\n						<li><a>340B</a></li>\n						<li><a>Telehealth</a></li>\n						<li><a>1115 States</a></li>\n						<li><a (click)="goToHomePage()">Sud</a></li>\n						<li><a>Profile</a></li>\n						<li><a>Logout</a></li>\n					</ul>\n				</div>\n				<h4>substance  use disorder</h4>\n			</div>\n		</div>\n	</header>\n	\n	\n	<!------>\n	\n	<!---section-->\n	<section>\n		\n		<div class="medi_coverage">\n			<img src="../../assets/images/energy.png">\n		</div>\n		<div class="sec_outr">\n			<div class="container">\n				<h5> State Telehealth Laws and Medicaid Policies: <br> 50-State Survey Findings</h5>\n				<h6>11.11.18</h6>\n				<a class="butttonn" href="">Download Attachment</a>\n				<div class="desprective-content">\n						 <ul class="research-img">\n								<li>\n								<a href="#">\n								 <img src="../../assets/images/Jacqueline .jpg">\n								</a>\n								 <div class="research_info">\n									 <h6>Jacqueline D. Marks</h6>  \n									 <p>MANAGER</p>\n								 </div>\n								</li>\n								<li>\n								<a href="#">\n								 <img src="../../assets/images/Augenstein_Jared.jpg">\n								</a>\n								 <div class="research_info">\n									 <h6>Jared Augenstein</h6>  \n									 <p> SR MANAGER</p>\n								 </div>\n								</li>\n								<li>\n								<a href="#">\n								 <img src="../../assets/images/Seigel_Randi.jpg">\n								</a>\n								 <div class="research_info">\n									 <h6>Randi Seigel</h6>  \n									 <p>Partner</p>\n								 </div>\n								</li>\n						 </ul>\n							<p>The use of telemedicine services is growing nationwide as providers and payors seek to improve access and better manage patient care, while reducing health care spending. State laws and Medicaid<sup><a style="color:undefined" href="javascript:void(0)" class="state_check" data-tab="sourcepara">[1]</a></sup> policies related to reimbursement, licensure and practice standards are rapidly evolving in response to the proliferation of technology and the growing evidence base that demonstrates the impact of telemedicine on access, quality and cost of care. Some states have been proactive in encouraging the use of telemedicine as a means to enhance services in rural areas, increase access to care for members with complex conditions, and reduce costs associated with unnecessary emergency department visits.</p>\n					 <p>In light of this rapidly changing landscape, Manatt Health has conducted a 50-state survey of state laws and Medicaid program policies related to telemedicine in the following key areas:</p>\n					 <ul class="prod_list">\n					 <li>Practice standards and licensure</li>\n					 <li>Coverage and reimbursement</li>\n					 <li>Eligible patient settings</li>\n					 <li>Eligible provider types</li>\n					 <li>Eligible technologies, and</li>\n					 <li>Service limitations.</li>\n					 </ul>\n					 <p>Based on survey results, we classified state telemedicine policies as \n						"progressive," "moderate," or "restrictive" across each of these categories, as captured in the state map below.</p>\n					 <p>Our analysis suggests that telemedicine will be critical to delivering health care in the future, and state Medicaid policies are evolving - in some states more quickly than others - to accelerate adoption of telemedicine models. As technology advances and the evidence base for telemedicine expands, state policy will continue to evolve to integrate telemedicine into payment and delivery reforms that support overarching program objectives related to access, quality, and cost of care.</p>\n					 <p>Additional findings from our analysis can be found here.</p>\n					 <p>To access a detailed summary profile of a state\'s telehealth laws and Medicaid policies, click the state on the map below. To explore state classifications across survey categories, use the State Classification Category drop down menu or hover your mouse over a state of interest. Explore further by using the controls at the top left of the map that allow you to zoom in/out, pan or type a state name in the search bar to quickly access a state of interest.</p>\n				 </div>\n			</div>\n			\n			<div class="dropdownn_Sec">\n				<div class="container">\n					<h5>Select States</h5>\n					<div class="dropdown_Sec">\n						<div class="container">\n							<ion-item>\n								<ion-label>States</ion-label>\n								 <ion-select name="statename" (ionChange)="onSelectChange($event)" >\n									<ion-option *ngFor="let state of states" value="{{state.abbreviation}}" >{{state.name}}</ion-option>\n								  </ion-select>\n							</ion-item>\n							<ion-item>\n								<ion-label>Categories</ion-label>\n								 <ion-select name="catname" (ionChange)="onCategoryChange($event)" >\n									<ion-option *ngFor="let cat of category" value="{{cat.title}}" >{{cat.title}}</ion-option>\n								  </ion-select>\n							</ion-item>\n						</div>\n					</div>\n					<div class="drop_Sec">\n						<i class="fas fa-question-circle"></i>\n						<i class="fas fa-globe-americas show_map" ></i>\n					</div>\n				</div>\n			</div>\n		</div>\n		\n		<div class="map_section">\n			<div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-mapp"></div>       \n		</div>\n		\n		<div id="state_data"></div>\n		\n	\n	</section>\n	<!--section-->\n	\n	\n	\n	<!----footer-->\n    <footer>\n		<div class="foot_section">\n			<img src="../../assets/images/logo.jpg">\n			<ul class="foot-nav">\n				<li><a>Privacy</a></li>\n				<li><a>340B</a></li>\n				<li><a>1115 States</a></li>\n			</ul>\n			<ul>\n				<li><a><i class="fab fa-facebook-f"></i></a></li>\n				<li><a><i class="fab fa-twitter"></i></a></li>\n				<li><a><i class="fab fa-linkedin-in"></i></a></li>\n			</ul>\n		</div>\n	</footer>\n	<!----footer-->\n	\n</ion-content>	'/*ion-inline-end:"C:\Users\DESTOP\Desktop\state\state-survay\src\pages\telehealth\telehealth.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], TelehealthPage);
    return TelehealthPage;
}());

//# sourceMappingURL=telehealth.js.map

/***/ }),

/***/ 113:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 113;

/***/ }),

/***/ 155:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 155;

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(221);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_telehealth_telehealth__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_rest_rest__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_telehealth_telehealth__["a" /* TelehealthPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], name: 'Home', segment: 'home' },
                        { component: __WEBPACK_IMPORTED_MODULE_5__pages_telehealth_telehealth__["a" /* TelehealthPage */], name: 'Telehealth', segment: 'telehealth' }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_telehealth_telehealth__["a" /* TelehealthPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_9__providers_rest_rest__["a" /* RestProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_telehealth_telehealth__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'telehealth', component: __WEBPACK_IMPORTED_MODULE_5__pages_telehealth_telehealth__["a" /* TelehealthPage */] },
            { title: '340B', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: '1115', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\state\state-survay\src\app\app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\DESTOP\Desktop\state\state-survay\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__telehealth_telehealth__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.getStates();
    }
    HomePage.prototype.goToTelehealthPage = function (pageName) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__telehealth_telehealth__["a" /* TelehealthPage */]);
    };
    HomePage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getState()
            .then(function (data) {
            _this.states = data;
            console.log(_this.states);
        });
    };
    ;
    HomePage.prototype.onSelectChange = function (selectedValue) {
        checkJson(selectedValue);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\DESTOP\Desktop\state\state-survay\src\pages\home\home.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<div class="logo">\n			<img src="../../assets/images/logo.jpg">\n		</div>\n	</ion-navbar>\n</ion-header>\n<ion-content> \n	<!---header--->\n	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">\n	\n	\n	\n	<header>\n		<div class="header_section">\n			<div class="container">\n				<div class="logo">\n					<img src="../../assets/images/logo.jpg">\n				</div>\n				\n				<div class="nav">\n				\n					<ul>\n						<li><a>340B</a></li>\n						<li><a (click)="goToTelehealthPage(\'TelehealthPage\')">Telehealth</a></li>\n						<li><a>1115 States</a></li>\n						<li><a>Sud</a></li>\n						<li><a>Profile</a></li>\n						<li><a>Logout</a></li>\n					</ul>\n				</div>\n				<h4>substance  use disorder</h4>\n			</div>\n		</div>\n	</header>\n	\n	\n	<!------>\n	\n	<!---section-->\n	<section>\n		\n		<div class="medi_coverage">\n			<img src="../../assets/images/energy.png">\n		</div>\n		<div class="sec_outr">\n			<div class="container">\n				<h5>Medicaid Coverage Waivers: State Profiles</h5>\n				<h6>11.11.18</h6>\n				<a class="butttonn" href="">Download Attachment</a>\n				<p>Section 1115 demonstrations permit states to waive certain Medicaid statutory requirements to advance state policy priorities and test innovations in their Medicaid programs, provided the Secretary of Health and Human Services determines that the demonstration “furthers the goals of the Medicaid program” and is budget neutral. States have used 1115 waiver authority to implement demonstrations ranging from Medicaid managed care programs to delivery system and payment reform initiatives. In recent years, states have leveraged 1115 waivers to modify features of Medicaid coverage for the expansion population under the Affordable Care Act (ACA) as well as traditional Medicaid populations.</p>\n				<p>The Trump Administration is encouraging states to pursue flexibility in administering their Medicaid programs through 1115 demonstrations that modify eligibility requirements and benefit design. In January 2018, the Centers for Medicare and Medicaid Services (CMS) released long-anticipated guidance on crafting and implementing a work and community engagement requirement as a condition of Medicaid eligibility. Following the release of the guidance, CMS approved 1115 waivers from Arkansas, Indiana, Kentucky, and New Hampshire, the first four waivers to permit such requirements in the Medicaid program, and on June 1, 2018, Arkansas became the first state to launch its work and community engagement requirement. The future of work and community engagement requirements is in question after a U.S. District Court judge issued a ruling in Stewart v. Azar in late June that invalidated HHS’s approval of Kentucky’s Medicaid waiver for failing to consider how the waiver furthered the goals of the Medicaid program. In August, Medicaid beneficiaries in Arkansas filed a lawsuit challenging HHS’s approval of that State’s Medicaid waiver to implement work and community engagement requirements.\n				</p>\n				<p>The Trump Administration has also recently begun to establish some guardrails on the changes to Medicaid that it will permit through 1115 waivers. In recent months, CMS has denied Kansas’s request to impose lifetime Medicaid enrollment limits and Massachusetts\'s request to waive Section 1927 to implement a closed prescription drug formulary, while continuing to receive the rebates required by federal law. Additionally, CMS has stated it will not approve Arkansas\'s or Massachusetts\'s request to implement partial expansion—in which it would reduce its Medicaid eligibility level to 100 percent of the federal poverty level, while continuing to receive the enhanced federal medical assistance percentage authorized under the ACA—\'at this time. The Administration\'s priorities will continue to emerge as CMS issues decisions on recently submitted state waiver proposals, including requests for work and community engagement requirements in non-expansion states, drug testing as a condition of Medicaid eligibility, and asset tests, among others.</p>\n				<p>This document inventories and compares the features of state Medicaid coverage waivers</p>	\n			</div>\n			\n			<div class="dropdownn_Sec">\n				<div class="container">\n					<h5>Select States</h5>\n					<div class="dropdown_Sec">\n						<div class="container">\n							<ion-item>\n						  <ion-label>States</ion-label>\n						  <ion-select name="statename" (ionChange)="onSelectChange($event)" >\n							<ion-option [disabled]="state.available == 0" *ngFor="let state of states" value="{{state.name}}" >{{state.name}}</ion-option>\n						  </ion-select>\n						</ion-item>\n						</div>\n					</div>\n					<div class="drop_Sec">\n						<i class="fas fa-question-circle"></i>\n						<i class="fas fa-chart-line show_chart"></i>\n						<i class="fas fa-globe-americas show_map" ></i>\n					</div>\n				</div>\n			</div>\n		</div>\n		\n		<div class="map_section">\n			<div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map"></div>       \n		</div>\n		\n		<div id="state_data"></div>\n		\n		<div class="toggllee">\n          <div class="jsmaps-wrapper-box"><div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map">\n			  </div>\n			</div>\n		</div>\n		  \n		<div class="table_hidee">\n			<div class="table_hide">\n				<div id="example-table-theme-semantic-ui"></div>\n				<div class="guide">\n					<ul>\n						<li><span><img src="../../blue.png"></span><h5>= Waiver obtained </h5></li>\n						<li><span><img src="../../green.png"></span><h5> = Waiver requested</h5></li>\n						<li><span><img src="../../red.png"></span><h5>= No waiver required</h5></li>\n					</ul>\n				</div>\n				<div class="table-controls">\n					\n					<button id="download-xlsx">Download XLSX</button>\n					\n				</div>\n			</div>\n		</div>\n		\n	</section>\n	<!--section-->\n	\n	\n	\n	<!----footer-->\n    <footer>\n		<div class="foot_section">\n			<img src="../../assets/images/logo.jpg">\n			<ul class="foot-nav">\n				<li><a>Privacy</a></li>\n				<li><a>340B</a></li>\n				<li><a>1115 States</a></li>\n			</ul>\n			<ul>\n				<li><a><i class="fab fa-facebook-f"></i></a></li>\n				<li><a><i class="fab fa-twitter"></i></a></li>\n				<li><a><i class="fab fa-linkedin-in"></i></a></li>\n			</ul>\n		</div>\n	</footer>\n	<!----footer-->\n	\n</ion-content>	'/*ion-inline-end:"C:\Users\DESTOP\Desktop\state\state-survay\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]) === "function" && _b || Object])
    ], HomePage);
    return HomePage;
    var _a, _b;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[200]);
//# sourceMappingURL=main.js.map