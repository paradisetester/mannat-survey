webpackJsonp([0],{

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_rest__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_seo_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var StatesPage = (function () {
    function StatesPage(nav, navCtrl, restProvider, http, auth, seo) {
        this.nav = nav;
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.http = http;
        this.auth = auth;
        seo.addTwitterCard('1115 States', '1115 States', 'Medicaid Coverage Waivers: State Profiles', '../../assets/images/logo.png');
        this.getStates();
    }
    StatesPage.prototype.ionViewCanEnter = function () {
        return this.auth.accesspage('1115states');
    };
    StatesPage.prototype.ionViewDidLoad = function () {
        var TIME_IN_MS = 500;
        var hideFooterTimeout = setTimeout(function () {
            USAmapLoad('usaTerritories3', '1115', '3');
        }, TIME_IN_MS);
    };
    StatesPage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getState1115()
            .then(function (data) {
            _this.states = data;
        });
    };
    ;
    StatesPage.prototype.onSelectChange = function (selectedValue) {
        checkJson1115(selectedValue);
    };
    StatesPage.prototype.logout = function () {
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    StatesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-states',template:/*ion-inline-start:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\states\states.html"*/'\n<ion-content class="page-home" cache-view="false" view-title="My Title!">\n	<my-main-header pagetitle="1115 States"></my-main-header>\n	<!------>\n	<section>\n		<div class="banner">\n			<img src="../../assets/images/statee.jpeg">\n			<div class="container">\n				<h1>1115 States</h1>\n			</div>\n		</div>\n	</section>\n\n	<!------>\n	<!---section-->\n\n\n	<section>\n		<div class="container">\n			<div class="outer">\n				<div class="left">\n					<h5>Medicaid Coverage Waivers: State Profiles</h5>\n					<a href="#" class="download_att_1115states">Dowload attachment</a>\n				</div>\n				<div class="right">\n				<img src="../../assets/images/right-img.png"></div>\n			</div>\n			<div class="content-medi">\n				<p>Section 1115 demonstrations permit states to waive certain Medicaid statutory requirements to advance state policy priorities and test innovations in their Medicaid programs, provided the Secretary of Health and Human Services determines that the demonstration "furthers the goals of the Medicaid program" and is budget neutral. States have used 1115 waiver authority to implement demonstrations ranging from Medicaid managed care programs to delivery system and payment reform initiatives. In recent years, states have leveraged 1115 waivers to modify features of Medicaid coverage for the expansion population under the Affordable Care Act (ACA) as well as traditional Medicaid populations.\n\nThe Trump Administration is encouraging states to pursue flexibility in administering their Medicaid programs through 1115 demonstrations that modify eligibility requirements and benefit design. In January 2018, the Centers for Medicare and Medicaid Services (CMS) released long-anticipated guidance on crafting and implementing a work and community engagement requirement as a condition of Medicaid eligibility. Following the release of the guidance, CMS approved 1115 waivers from Arkansas, Indiana, Kentucky, and New Hampshire, the first four waivers to permit such requirements in the Medicaid program, and on June 1, 2018, Arkansas became the first state to launch its work and community engagement requirement. The future of work and community engagement requirements is in question after a U.S. District Court judge issued a ruling in Stewart v. Azar in late June that invalidated HHS\'s approval of Kentucky\'s Medicaid waiver for failing to consider how the waiver furthered the goals of the Medicaid program. In August, Medicaid beneficiaries in Arkansas filed a lawsuit challenging HHS\'s approval of that State\'s Medicaid waiver to implement work and community engagement requirements.\n\nThe Trump Administration has also recently begun to establish some guardrails on the changes to Medicaid that it will permit through 1115 waivers. In recent months, CMS has denied Kansas\'s request to impose lifetime Medicaid enrollment limits and Massachusetts\'s request to waive Section 1927 to implement a closed prescription drug formulary, while continuing to receive the rebates required by federal law. Additionally, CMS has stated it will not approve Arkansas\'s or Massachusetts\'s request to implement partial expansion-in which it would reduce its Medicaid eligibility level to 100 percent of the federal poverty level, while continuing to receive the enhanced federal medical assistance percentage authorized under the ACA-"at this time." The Administration\'s priorities will continue to emerge as CMS issues decisions on recently submitted state waiver proposals, including requests for work and community engagement requirements in non-expansion states, drug testing as a condition of Medicaid eligibility, and asset tests, among others.\n\nThis document inventories and compares the features of state Medicaid coverage waivers.</p>\n			</div>\n		</div>\n\n	</section>\n	<section class="state-drop-sec">\n		<div class="container">\n			<div class="state-inner">\n				<h5>Select States</h5>\n\n								 <select class="select_state_1115states" name="statename" >\n									<option *ngFor="let state of states" value="{{state.name}}"  [disabled]="state.available == 0"  >{{state.name}}</option>\n								  </select>\n\n				<div class="icons-sec">\n					<ul>\n					<li><a href="" class="qus_1115states"><i class="fas fa-question"></i></a></li>\n						<li><a  class="chart_1115states" onclick="show_chart(3)"><i class="fas fa-chart-line " ></i></a></li>\n						<li><a class="map_1115states" onclick="show_map(3)"><i class="fas fa-globe-americas" ></i></a></li>\n					</ul>\n				</div>\n			</div>\n		</div>\n	</section>\n\n	<section>\n		<div class="map_section">\n			<div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map3"></div>\n		</div>\n\n		<div id="state_data"></div>\n\n		<div class="table_hidee">\n			<div class="table_hide">\n				<div id="example-table-theme-semantic-ui"></div>\n				<div class="guide">\n					<ul>\n						<li><span><img src="../../blue.png"></span><h5>= Waiver obtained </h5></li>\n						<li><span><img src="../../green.png"></span><h5> = Waiver requested</h5></li>\n						<li><span><img src="../../red.png"></span><h5>= No waiver required</h5></li>\n					</ul>\n				</div>\n				<div class="table-controls">\n\n					<button id="download-xlsx">Download XLSX</button>\n\n				</div>\n			</div>\n		</div>\n\n	</section>\n	<!--section-->\n	<!----footer-->\n    <footer>\n		<div class="container">\n			<div class="foot-outr">\n				<div class="left-foot">\n					<img src="">\n				</div>\n				<div class="right-foot">\n					<ul>\n						<li><a href="">Privacy</a></li>\n						<li><a href="/#/340b">340B</a></li>\n						<li><a href="/#/1115states" >1115 States</a></li>\n					</ul>\n				</div>\n			</div>\n			<div class="lower-foot">\n				<ul>\n					<li>Follow us:</li>\n					<li>\n						<a href=""><i class="fab fa-facebook"></i> </a>\n						<a href=""><i class="fab fa-twitter"></i></a>\n						<a href=""><i class="fab fa-linkedin-in"></i></a>\n					</li>\n				</ul>\n			</div>\n		</div>\n	</footer>\n	<!----footer-->\n\n</ion-content>\n'/*ion-inline-end:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\states\states.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_6__services_seo_service__["a" /* SeoService */]])
    ], StatesPage);
    return StatesPage;
}());

//# sourceMappingURL=states.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FourtybPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_rest__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_seo_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var FourtybPage = (function () {
    function FourtybPage(nav, navCtrl, restProvider, http, auth, seo) {
        this.nav = nav;
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.http = http;
        this.auth = auth;
        this.getStates();
        seo.addTwitterCard('340B 50 State Survey', '340B 50 State Survey', '340B 50 State Survey', '../../assets/images/logo.png');
    }
    FourtybPage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getState340b()
            .then(function (data) {
            _this.states = data;
        });
    };
    ;
    FourtybPage.prototype.onSelectChange = function (selectedValue) {
        checkJson340b(selectedValue);
    };
    FourtybPage.prototype.ionViewCanEnter = function () {
        return this.auth.accesspage('340b');
    };
    FourtybPage.prototype.ionViewDidLoad = function () {
        var TIME_IN_MS = 500;
        var hideFooterTimeout = setTimeout(function () {
            USAmapLoad('usaTerritories4', '340b', '4');
        }, TIME_IN_MS);
    };
    FourtybPage.prototype.logout = function () {
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    FourtybPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fourtyb',template:/*ion-inline-start:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\fourtyb\fourtyb.html"*/'\n<ion-content class="page-home" cache-view="false" view-title="My Title!">\n<my-main-header pagetitle="340B"></my-main-header>\n	<!------>\n	<section>\n		<div class="banner">\n			<img src="../../assets/images/fourtyb.jpeg">\n			<div class="container">\n				<h1>340B</h1>\n			</div>\n		</div>\n	</section>\n\n	<!------>\n		<section>\n		<div class="container">\n			<div class="outer">\n				<div class="left">\n					<h5> 340B 50 State Survey</h5>\n					<a href="#" class="download_att_fourth">Dowload attachment</a>\n				</div>\n				<div class="right">\n				<img src="../../assets/images/right-img.png"></div>\n			</div>\n			<div class="content-medi">\n				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries\n\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>\n			</div>\n		</div>\n\n	</section>\n\n	<section class="state-drop-sec">\n		<div class="container">\n			<div class="state-inner">\n				<h5>Select States</h5>\n					 <select class="select_state_fourth"  name="statename"  >\n						<option *ngFor="let state of states" value="{{state.name}}"   [disabled]="state.available == 0" >{{state.name}}</option>\n					  </select>\n\n				<div class="icons-sec">\n					<ul>\n						<li><a href="" class="qus_fourty" ><i class="fas fa-question"></i></a></li>\n					<li><a class="map_fourty" onclick="show_map(4)"><i class="fas fa-globe-americas"></i></a></li>\n					</ul>\n				</div>\n			</div>\n		</div>\n	</section>\n	<!---section-->\n	<section>\n\n		 <div class="container">\n		<div class="map_section">\n			<div class="jsmaps-wrapper-box"><div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map4"></div>       </div>\n		</div>\n\n\n		<div class="material_outr">\n					<div id="state_data"></div>\n				</div>\n		 </div>\n	</section>\n	<!--section-->\n\n\n	<!----footer-->\n    <footer>\n		<div class="container">\n			<div class="foot-outr">\n				<div class="left-foot">\n					<img src="">\n				</div>\n				<div class="right-foot">\n					<ul>\n						<li><a href="">Privacy</a></li>\n						<li><a href="/#/340b">340B</a></li>\n						<li><a href="/#/1115states" >1115 States</a></li>\n					</ul>\n				</div>\n			</div>\n			<div class="lower-foot">\n				<ul>\n					<li>Follow us:</li>\n					<li>\n						<a href=""><i class="fab fa-facebook"></i> </a>\n						<a href=""><i class="fab fa-twitter"></i></a>\n						<a href=""><i class="fab fa-linkedin-in"></i></a>\n					</li>\n				</ul>\n			</div>\n		</div>\n	</footer>\n	<!----footer-->\n\n</ion-content>\n'/*ion-inline-end:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\fourtyb\fourtyb.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_6__services_seo_service__["a" /* SeoService */]])
    ], FourtybPage);
    return FourtybPage;
}());

//# sourceMappingURL=fourtyb.js.map

/***/ }),

/***/ 210:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 210;

/***/ }),

/***/ 264:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 264;

/***/ }),

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return firebaseConfig; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Config = (function () {
    function Config() {
        this.ApiUrl = '';
    }
    Config = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], Config);
    return Config;
}());

;
var firebaseConfig = {
    fire: {
        apiKey: "AIzaSyCM8CzA0KxPnpo7eB_ujevMLYu6s8G6ys8",
        authDomain: "mannat-survey.firebaseapp.com",
        databaseURL: "https://mannat-survey.firebaseio.com",
        projectId: "mannat-survey",
        storageBucket: "mannat-survey.appspot.com",
        messagingSenderId: "830917101058"
    }
};
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_seo_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__profile_profile__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SignupPage = (function () {
    function SignupPage(fb, navCtrl, auth, store, seo) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.store = store;
        this.authState = null;
        this.selectedArray = [];
        this.checkboxpages = [];
        this.datachecbox = [];
        seo.addTwitterCard('Sign Up', 'Sign Up', 'Sign Up', '../../assets/images/logo.png');
        this.form = fb.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].email])],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].minLength(6)])],
        });
        this.checkboxpages = [
            { pageid: 1, name: "sud", checked: false },
            { pageid: 2, name: "telehealth", checked: false },
            { pageid: 3, name: "1115states", checked: false },
            { pageid: 4, name: "340b", checked: false }
        ];
    }
    SignupPage.prototype.selectpage = function (data) {
        if (data.checked == true) {
            this.selectedArray.push(data);
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.pageid !== data.pageid;
            });
            this.selectedArray = newArray;
        }
        console.log(this.selectedArray);
    };
    SignupPage.prototype.signup = function () {
        var _this = this;
        this.signupError = "processing...";
        var data = this.form.value;
        this.selectedArray.forEach(function (page) {
            if (page.checked == true) {
                _this.datachecbox.push(page.name);
            }
        });
        var credentials = {
            email: data.email,
            password: data.password,
            pages: JSON.stringify(this.datachecbox)
        };
        this.auth.signUp(credentials).then(function (user) {
            _this.authState = user;
            _this.auth.updateUserData(_this.authState.user.uid, credentials.pages);
            _this.authState.user.sendEmailVerification();
            _this.store.set('seelcted_page', JSON.stringify(_this.datachecbox));
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__profile_profile__["a" /* ProfilePage */]);
        })
            .catch(function (error) { _this.signupError = error.message; });
    };
    SignupPage.prototype.loginbtn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'as-page-signup',template:/*ion-inline-start:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\signup\signup.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Signup</ion-title>\n		</ion-navbar>\n</ion-header>\n\n<ion-content class="page-signup">\n	\n<section class="form-section">\n	<div class="form-inner">\n		<div class="form-graphic">\n			<img src="../../assets/images/vector.png">\n		</div>\n		<div class="form-design">\n			<img src="../../assets/images/logo.png">\n			\n			<form (ngSubmit)="signup()" [formGroup]="form" autocomplete="off">\n				<p class="form_heading">Signup</p>\n				<ion-list inset>\n					\n					<ion-item [ngClass]="{ invalid: emailErrors.hasError(\'*\', [\'touched\']) }">\n						<ion-input type="text" placeholder="Email" formControlName="email" autocomplete="on" ></ion-input>\n					</ion-item>\n\n					<div ngxErrors="email" #emailErrors="ngxErrors">\n						<div [ngxError]="[\'email\', \'required\']" [when]="[\'touched\']">It should be a valid email</div>\n					</div>\n\n					<ion-item [ngClass]="{ invalid: passwordErrors.hasError(\'*\', [\'touched\']) }">\n						<ion-input type="password" placeholder="Password" formControlName="password"></ion-input>\n					</ion-item>\n\n					<div ngxErrors="password" #passwordErrors="ngxErrors">\n						<div [ngxError]="[\'minlength\', \'required\']" [when]="[\'touched\']">It should be at least 6 characters</div>\n					</div>\n					\n					<div class="checkbox_list">\n					<h4>Looking For</h4>\n					<ion-list class="chk-bx">\n	<ion-item *ngFor="let page of checkboxpages"><ion-checkbox color="primary" (click)="selectpage(page)"  [(ngModel)]="page.checked" [ngModelOptions]="{standalone: true}"></ion-checkbox>\n	<ion-label>{{page.name}}</ion-label></ion-item>\n	</ion-list>\n					</div>\n				</ion-list>\n\n				<div padding-horizontal>\n					<div class="form-error">{{signupError}}</div>\n\n					<button ion-button full type="submit" [disabled]="!form.valid">Sign up</button>\n				\n				\n				<ion-list class="margin-top login-si">\n\n					<button ion-button icon-left block login-si clear (click)="loginbtn()">\n						<ion-icon name="person-add"></ion-icon>\n						Login\n					</button>\n				</ion-list>\n				</div>\n			</form>\n			\n		</div>\n	</div>\n</section>\n\n</ion-content>'/*ion-inline-end:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\signup\signup.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__services_seo_service__["a" /* SeoService */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(436);



Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase_app__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_firestore__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_firestore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthService = (function () {
    function AuthService(afAuth, firestore, store) {
        var _this = this;
        this.afAuth = afAuth;
        this.firestore = firestore;
        this.store = store;
        this.authState = null;
        this.userIDresponse = null;
        afAuth.authState.subscribe(function (user) {
            _this.user = user;
        });
    }
    AuthService.prototype.signInWithEmail = function (credentials) {
        var _this = this;
        console.log('Sign in with email');
        return this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password).then(function (user) {
            _this.authState = user;
            _this.userscollection = _this.firestore.collection('mannat');
            _this.userscollection.snapshotChanges().map(function (changes) {
                return changes.map(function (a) {
                    var data = a.payload.doc.data();
                    data.id = a.payload.doc.id;
                    return data;
                });
            }).subscribe(function (Use) {
                Use.forEach(function (doc) {
                    if (doc.id == _this.authState.user.uid) {
                        _this.store.set('seelcted_page', doc.pages);
                    }
                });
            });
        });
    };
    AuthService.prototype.accesspage = function (currentpage) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                _this.store.get('seelcted_page').then(function (val) {
                    if (val) {
                        resolve((JSON.parse(val).indexOf(currentpage) > -1));
                    }
                    else {
                        resolve(true);
                    }
                });
            }, 10);
        });
    };
    AuthService.prototype.getUid = function () {
        return this.user && this.user.uid;
    };
    AuthService.prototype.signUp = function (credentials) {
        return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
    };
    AuthService.prototype.updateUserData = function (uid, pages) {
        this.firestore.doc('mannat/' + uid).set({
            uid: uid,
            pages: pages
        });
    };
    Object.defineProperty(AuthService.prototype, "authenticated", {
        get: function () {
            return this.user !== null;
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.getEmail = function () {
        return this.user && this.user.email;
    };
    AuthService.prototype.signOut = function () {
        return this.afAuth.auth.signOut();
    };
    AuthService.prototype.signInWithGoogle = function () {
        console.log('Sign in with google');
        return this.oauthSignIn(new __WEBPACK_IMPORTED_MODULE_2_firebase_app__["auth"].GoogleAuthProvider());
    };
    AuthService.prototype.oauthSignIn = function (provider) {
        if (!window.cordova) {
            return this.afAuth.auth.signInWithPopup(provider);
        }
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"], __WEBPACK_IMPORTED_MODULE_3_angularfire2_firestore__["AngularFirestore"], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], AuthService);
    return AuthService;
}());

//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__agm_core__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home_page__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_telehealth_telehealth__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_states_states__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_fourtyb_fourtyb__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__app_component__ = __webpack_require__(518);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angularfire2__ = __webpack_require__(519);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_angularfire2_auth__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_auth_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ultimate_ngxerrors__ = __webpack_require__(520);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_rest__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__angular_common_http__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_common__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_my_main_header_my_main_header__ = __webpack_require__(531);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__services_seo_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_angularfire2_firestore__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_angularfire2_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_24_angularfire2_firestore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_storage__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home_page__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_telehealth_telehealth__["a" /* TelehealthPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_states_states__["a" /* StatesPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_fourtyb_fourtyb__["a" /* FourtybPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_22__components_my_main_header_my_main_header__["a" /* MyMainHeaderComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_20__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { component: __WEBPACK_IMPORTED_MODULE_7__pages_home_home_page__["a" /* HomePage */], name: 'Home', segment: '' },
                        { component: __WEBPACK_IMPORTED_MODULE_8__pages_telehealth_telehealth__["a" /* TelehealthPage */], name: 'Telehealth', segment: 'telehealth' },
                        { component: __WEBPACK_IMPORTED_MODULE_9__pages_states_states__["a" /* StatesPage */], name: 'States', segment: '1115states' },
                        { component: __WEBPACK_IMPORTED_MODULE_12__pages_fourtyb_fourtyb__["a" /* FourtybPage */], name: 'Fourty', segment: '340b' },
                        { component: __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */], name: 'Login', segment: 'login' },
                        { component: __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__["a" /* SignupPage */], name: 'Signup', segment: 'signup' },
                        { component: __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */], name: 'Profile', segment: 'profile' }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_0__agm_core__["a" /* AgmCoreModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_15_angularfire2__["AngularFireModule"].initializeApp(__WEBPACK_IMPORTED_MODULE_6__config__["b" /* firebaseConfig */].fire),
                __WEBPACK_IMPORTED_MODULE_24_angularfire2_firestore__["AngularFirestoreModule"],
                __WEBPACK_IMPORTED_MODULE_25__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_18__ultimate_ngxerrors__["a" /* NgxErrorsModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_telehealth_telehealth__["a" /* TelehealthPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home_page__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_states_states__["a" /* StatesPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_fourtyb_fourtyb__["a" /* FourtybPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__config__["a" /* Config */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_16_angularfire2_auth__["AngularFireAuth"],
                __WEBPACK_IMPORTED_MODULE_17__services_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_19__services_rest__["a" /* RestProvider */],
                __WEBPACK_IMPORTED_MODULE_23__services_seo_service__["a" /* SeoService */],
                __WEBPACK_IMPORTED_MODULE_24_angularfire2_firestore__["AngularFirestore"],
                { provide: __WEBPACK_IMPORTED_MODULE_21__angular_common__["f" /* LocationStrategy */], useClass: __WEBPACK_IMPORTED_MODULE_21__angular_common__["g" /* PathLocationStrategy */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home_page__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__signup_signup__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_seo_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LoginPage = (function () {
    function LoginPage(navCtrl, auth, _location, fb, seo) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this._location = _location;
        seo.addTwitterCard('Login', 'Login', 'Login', '../../assets/images/logo.png');
        this.loginForm = fb.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].email])],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].minLength(6)])]
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        var TIME_IN_MS = 500;
        var hideFooterTimeout = setTimeout(function () {
            /* 	  var emails = getUrlParameter('email');
                  var password = getUrlParameter('token');
                 if(emails){
                        this.autoLogin(emails,password);
                     } */
        }, TIME_IN_MS);
    };
    LoginPage.prototype.autoLogin = function (email, password) {
        var _this = this;
        var credentials = {
            email: email,
            password: password
        };
        this.auth.signInWithEmail(credentials)
            .then(function () { return _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__profile_profile__["a" /* ProfilePage */]); }, function (error) { return _this.loginError = error.message; });
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        var data = this.loginForm.value;
        if (!data.email) {
            return;
        }
        var credentials = {
            email: data.email,
            password: data.password
        };
        this.auth.signInWithEmail(credentials)
            .then(function () { return _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__profile_profile__["a" /* ProfilePage */]); }, function (error) { return _this.loginError = error.message; });
    };
    LoginPage.prototype.loginWithGoogle = function () {
        var _this = this;
        this.auth.signInWithGoogle()
            .then(function () { return _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home_page__["a" /* HomePage */]); }, function (error) { return console.log(error.message); });
    };
    LoginPage.prototype.signupbtn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__signup_signup__["a" /* SignupPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\login\login.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Log in</ion-title>\n	</ion-navbar>\n</ion-header>\n\n<ion-content>\n	\n<section class="form-section">\n	<div class="form-inner">\n		<div class="form-graphic">\n			<img src="../../assets/images/vector.png">\n		</div>\n		<div class="form-design">\n			<img src="../../assets/images/logo.png">\n		 \n			<form (ngSubmit)="login()" [formGroup]="loginForm">\n				<p class="form_heading">Login</p>\n				\n				\n		<ion-list inset>\n\n			<ion-item [ngClass]="{ invalid: emailErrors.hasError(\'*\', [\'touched\', \'dirty\']) }">\n				<ion-input type="text" placeholder="Email" formControlName="email"></ion-input>\n			</ion-item>\n\n			<div ngxErrors="email" #emailErrors="ngxErrors">\n				<div [ngxError]="[\'email\', \'required\']" [when]="[\'touched\', \'dirty\']">It should be a valid email</div>\n			</div>\n\n			<ion-item [ngClass]="{ invalid: passwordErrors.hasError(\'*\', [\'touched\']) }">\n				<ion-input type="password" placeholder="Password" formControlName="password"></ion-input>\n			</ion-item>\n\n			<div ngxErrors="password" #passwordErrors="ngxErrors">\n				<div [ngxError]="[\'minlength\', \'required\']" [when]="[\'touched\']">It should be at least 5 characters</div>\n			</div>\n		</ion-list>\n\n		<div padding-horizontal>\n			<div class="form-error">{{loginError}}</div>\n\n			<button ion-button full type="submit" [disabled]="!loginForm.valid">Log in</button>\n			<!--div class="login-footer">\n				<p>\n					<a href="#">Forgot password?</a>\n					If you\'re a new user, please sign up.\n				</p>\n			</div--->\n\n			\n			<ion-list>\n\n				<button ion-button icon-left block clear (click)="signupbtn()">\n					<ion-icon name="person-add"></ion-icon>\n					Sign up\n				</button>\n			</ion-list>\n		</div>\n	</form>\n			\n		</div>\n	</div>\n</section>\n\n</ion-content>'/*ion-inline-end:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_7__angular_common__["e" /* Location */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_8__services_seo_service__["a" /* SeoService */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SeoService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SeoService = (function () {
    function SeoService(title, meta) {
        this.title = title;
        this.meta = meta;
    }
    SeoService.prototype.addTwitterCard = function (title, keywords, description, img) {
        // Set HTML Document Title
        this.title.setTitle(title);
        // Add Twitter Card Metatags
        this.meta.updateTag({ name: 'title', content: title });
        this.meta.updateTag({ name: 'keywords', content: keywords });
        this.meta.updateTag({ name: 'description', content: description });
        this.meta.updateTag({ name: 'image', content: img });
    };
    SeoService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["f" /* Title */], __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["e" /* Meta */]])
    ], SeoService);
    return SeoService;
}());

//# sourceMappingURL=seo-service.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_home_home_page__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_telehealth_telehealth__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_states_states__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_fourtyb_fourtyb__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_auth_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var MyApp = (function () {
    function MyApp(platform, menu, statusBar, store, auth) {
        // set our app's pages
        var _this = this;
        this.platform = platform;
        this.menu = menu;
        this.statusBar = statusBar;
        this.store = store;
        this.auth = auth;
        this.store.get('seelcted_page').then(function (val) {
            _this.pages = [];
            if (val) {
                if ((JSON.parse(val).indexOf('sud') > -1)) {
                    _this.pages.push({ title: 'Home', component: __WEBPACK_IMPORTED_MODULE_3__pages_home_home_page__["a" /* HomePage */] });
                }
                if ((JSON.parse(val).indexOf('telehealth') > -1)) {
                    _this.pages.push({ title: 'Telehealth', component: __WEBPACK_IMPORTED_MODULE_4__pages_telehealth_telehealth__["a" /* TelehealthPage */] });
                }
                if ((JSON.parse(val).indexOf('1115states') > -1)) {
                    _this.pages.push({ title: '1115 States', component: __WEBPACK_IMPORTED_MODULE_5__pages_states_states__["a" /* StatesPage */] });
                }
                if ((JSON.parse(val).indexOf('340b') > -1)) {
                    _this.pages.push({ title: '340B', component: __WEBPACK_IMPORTED_MODULE_6__pages_fourtyb_fourtyb__["a" /* FourtybPage */] });
                }
                _this.pages.push({ title: 'profile', component: __WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__["a" /* ProfilePage */] });
            }
        });
        this.initializeApp(this.pages);
        //this.rootPage = LoginPage;
    }
    MyApp.prototype.initializeApp = function (page) {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
        });
        this.auth.afAuth.authState
            .subscribe(function (user) {
            if (user) {
                _this.rootPage = page.component;
            }
            else {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
            }
        }, function () {
            alert(123);
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
        });
    };
    MyApp.prototype.login = function () {
        this.menu.close();
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
    };
    MyApp.prototype.logout = function () {
        this.menu.close();
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_home_home_page__["a" /* HomePage */]);
    };
    MyApp.prototype.openPage = function (page) {
        var _this = this;
        this.menu.close();
        this.auth.afAuth.authState
            .subscribe(function (user) {
            if (user) {
                _this.nav.setRoot(page.component);
            }
            else {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
            }
        }, function () {
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n	<ion-content>\n\n		<ion-list>\n\n			<ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n				<ion-icon [name]="p.icon" item-left></ion-icon>\n\n				{{p.title}}\n\n			</ion-item>\n\n			<ion-list-header *ngIf="auth.getEmail()">{{auth.getEmail()}}</ion-list-header>\n\n\n\n<ion-item (click)="logout()" *ngIf="auth.authenticated">\n\n	<ion-icon name="log-out" item-left></ion-icon>\n\n	Log out\n\n</ion-item>\n\n\n\n<ion-item (click)="login()" *ngIf="!auth.authenticated">\n\n	<ion-icon name="log-in" item-left></ion-icon>\n\n	Log in\n\n</ion-item>\n\n		</ion-list>\n\n	</ion-content>\n\n\n\n</ion-menu>\n\n	\n\n<ion-nav id="nav" [root]="rootPage" #content swipe-back-enabled="false"></ion-nav>\n\n'/*ion-inline-end:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_9__services_auth_service__["a" /* AuthService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 531:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyMainHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_home_home_page__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_telehealth_telehealth__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_states_states__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_fourtyb_fourtyb__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_profile_profile__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_auth_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the MyMainHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var MyMainHeaderComponent = (function () {
    function MyMainHeaderComponent(navCtrl, auth, menu, store) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.menu = menu;
        this.store = store;
        this.store.get('seelcted_page').then(function (val) {
            _this.pages = [];
            if (val) {
                if ((JSON.parse(val).indexOf('sud') > -1)) {
                    _this.pages.push({ title: 'Home', component: __WEBPACK_IMPORTED_MODULE_2__pages_home_home_page__["a" /* HomePage */] });
                }
                if ((JSON.parse(val).indexOf('telehealth') > -1)) {
                    _this.pages.push({ title: 'Telehealth', component: __WEBPACK_IMPORTED_MODULE_3__pages_telehealth_telehealth__["a" /* TelehealthPage */] });
                }
                if ((JSON.parse(val).indexOf('1115states') > -1)) {
                    _this.pages.push({ title: '1115 States', component: __WEBPACK_IMPORTED_MODULE_4__pages_states_states__["a" /* StatesPage */] });
                }
                if ((JSON.parse(val).indexOf('340b') > -1)) {
                    _this.pages.push({ title: '340B', component: __WEBPACK_IMPORTED_MODULE_5__pages_fourtyb_fourtyb__["a" /* FourtybPage */] });
                }
                _this.pages.push({ title: 'profile', component: __WEBPACK_IMPORTED_MODULE_7__pages_profile_profile__["a" /* ProfilePage */] });
            }
        });
        this.title = false;
    }
    MyMainHeaderComponent.prototype.ngAfterViewInit = function () {
        this.title = this.textToUse;
    };
    MyMainHeaderComponent.prototype.login = function () {
        this.menu.close();
        this.auth.signOut();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */]);
    };
    MyMainHeaderComponent.prototype.logout = function () {
        this.menu.close();
        this.auth.signOut();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */]);
    };
    MyMainHeaderComponent.prototype.openPagemain = function (page) {
        var _this = this;
        this.menu.close();
        this.auth.afAuth.authState
            .subscribe(function (user) {
            if (user) {
                _this.navCtrl.setRoot(page.component);
            }
            else {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
            }
        }, function () {
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */])
    ], MyMainHeaderComponent.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('pagetitle'),
        __metadata("design:type", Object)
    ], MyMainHeaderComponent.prototype, "textToUse", void 0);
    MyMainHeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'my-main-header',template:/*ion-inline-start:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\components\my-main-header\my-main-header.html"*/'\n<ion-header>\n\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<div class="logo">\n			<img src="../../assets/images/logo.webp">\n		</div>\n	</ion-navbar>\n</ion-header>\n\n	<!---header--->\n	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">\n\n\n\n	<header><div class="userID">{{auth.getUid()}}</div>\n		<div class="top_bar">\n			<div class="container">\n				<ul>\n					<li><a href="mailto:insights@manatt.com "><i class="fas fa-envelope"></i>insights@manatt.com </a></li>\n					<li><a href="tel:0987654321"><i class="fas fa-phone"></i>(098) 765-4321</a></li>\n					<li>Get In Touch\n						<a href="#"><i class="fab fa-facebook"></i></a>\n						<a href="#"><i class="fab fa-twitter-square"></i></a>\n						<a href="#"><i class="fab fa-google-plus-square"></i></a>\n					</li>\n				</ul>\n			</div>\n			</div>\n		<div class="container">\n			<div class="lower_bar">\n				<ul>\n	<li *ngFor="let p of pages" [class.active_menu]="title == p.title"  (click)="openPagemain(p)"  ><a>{{p.title}}</a>\n\n					</li>\n\n\n					<li *ngIf="auth.authenticated">\n						<a (click)="logout()">Log out</a>\n					</li>\n					<li *ngIf="!auth.authenticated">\n						<a (click)="login()">Log in</a>\n					</li>\n				</ul>\n			</div>\n			<div class="logo">\n				<img src="../../assets/images/logo.png">\n			</div>\n		</div>\n	</header>\n'/*ion-inline-end:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\components\my-main-header\my-main-header.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_8__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */]])
    ], MyMainHeaderComponent);
    return MyMainHeaderComponent;
}());

//# sourceMappingURL=my-main-header.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_firestore__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_firestore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams, fb, auth, afAuth, store, firestore) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.afAuth = afAuth;
        this.store = store;
        this.firestore = firestore;
        this.selectedArray = [];
        this.checkboxpages = [];
        this.datachecbox = [];
        this.userdata = [];
        this.Getalluserdata();
        this.profileForm = fb.group({
            fname: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].email])],
            lname: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required])],
            email: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required])],
            phone_number: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required])],
            pages: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required])],
        });
        this.checkboxpages = [
            { pageid: 1, name: "sud", checked: false },
            { pageid: 2, name: "telehealth", checked: false },
            { pageid: 3, name: "1115states", checked: false },
            { pageid: 4, name: "340b", checked: false }
        ];
    }
    ProfilePage.prototype.selectpage = function (data) {
        if (data.checked == true) {
            // this.selectedArray.push(data);
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.pageid !== data.pageid;
            });
            //this.selectedArray = newArray;
        }
        //console.log(this.selectedArray);
    };
    ProfilePage.prototype.Getalluserdata = function () {
        var _this = this;
        this.userscollection = this.firestore.collection('mannat');
        this.userscollection.snapshotChanges().map(function (changes) {
            return changes.map(function (a) {
                var data = a.payload.doc.data();
                data.id = a.payload.doc.id;
                return data;
            });
        }).subscribe(function (Use) {
            Use.forEach(function (doc) {
                if (doc.id == _this.auth.getUid()) {
                    _this.userdata = doc;
                    var userpaegsdata = JSON.parse(_this.userdata.pages);
                    _this.checkboxpages = [
                        { pageid: 1, name: "sud", checked: (userpaegsdata.indexOf('sud') > -1) },
                        { pageid: 2, name: "telehealth", checked: (userpaegsdata.indexOf('telehealth') > -1) },
                        { pageid: 3, name: "1115states", checked: (userpaegsdata.indexOf('1115states') > -1) },
                        { pageid: 4, name: "340b", checked: (userpaegsdata.indexOf('340b') > -1) }
                    ];
                    _this.selectedArray = _this.checkboxpages;
                }
            });
        });
    };
    ProfilePage.prototype.profile_form = function () {
        var _this = this;
        var data = this.profileForm.value;
        this.selectedArray.forEach(function (page) {
            if (page.checked == true) {
                _this.datachecbox.push(page.name);
            }
        });
        if (!data.email) {
            return;
        }
        this.firestore.doc('mannat/' + this.auth.getUid()).set({
            uid: this.auth.getUid(),
            fname: data.fname,
            pages: JSON.stringify(this.datachecbox),
            lname: data.lname,
            email: data.email,
            phoneumber: data.phone_number,
        });
        this.store.set('seelcted_page', JSON.stringify(this.datachecbox));
        this.datachecbox = [];
    };
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\profile\profile.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Profiles</ion-title>\n	</ion-navbar>\n</ion-header>\n\n<ion-content>\n<my-main-header  pagetitle="profile"></my-main-header>\n\n\n<form (ngSubmit)="profile_form()" [formGroup]="profileForm">\n<div class="profile-form form-design">\n<ion-list>\n  <ion-item>\n  <h4>First Name</h4>\n    <ion-input  placeholder="First Name" formControlName="fname" value="{{userdata.fname}}"></ion-input>\n	\n  </ion-item>\n<ion-item>\n<h4>Last Name</h4>\n     <ion-input placeholder="Last Name" formControlName="lname" value="{{userdata.lname}}"></ion-input>\n</ion-item>\n<ion-item>\n<h4>Email</h4>\n     <ion-input  placeholder="Email" formControlName="email" value="{{userdata.email}}"></ion-input>\n	\n</ion-item>\n<ion-item>\n<h4>Phone Number</h4>\n     <ion-input placeholder="Phone Number" formControlName="phone_number" value="{{userdata.phoneumber}}"> </ion-input>\n</ion-item>\n\n</ion-list>\n\n<ion-list class="chk-bx">\n	<ion-item *ngFor="let page of checkboxpages">\n	<ion-checkbox color="primary" (click)="selectpage(page)"  [(ngModel)]="page.checked" [ngModelOptions]="{standalone: true}"></ion-checkbox>\n	<ion-label>{{page.name}}</ion-label></ion-item>\n	\n	\n \n</ion-list>\n\n<button ion-button full type="submit" >Save</button>\n</div>\n\n</form>\n\n<!----footer-->\n    <footer>\n		<div class="container">\n			<div class="foot-outr">\n				<div class="left-foot">\n					<img src="">\n				</div>\n				<div class="right-foot">\n					<ul>\n						<li><a href="">Privacy</a></li>\n						<li><a href="/#/340b">340B</a></li>\n						<li><a href="/#/1115states" >1115 States</a></li>\n					</ul>\n				</div>\n			</div>\n			<div class="lower-foot">\n				<ul>\n					<li>Follow us:</li>\n					<li>\n						<a href=""><i class="fab fa-facebook"></i> </a>\n						<a href=""><i class="fab fa-twitter"></i></a>\n						<a href=""><i class="fab fa-linkedin-in"></i></a>\n					</li>\n				</ul>\n			</div>\n		</div>\n	</footer>\n	<!----footer-->\n\n</ion-content>'/*ion-inline-end:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_firestore__["AngularFirestore"]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestProvider = (function () {
    function RestProvider(http) {
        this.http = http;
        this.apiUrl = 'assets/js/';
        console.log('API enable');
    }
    RestProvider.prototype.getState = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'sudstates.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getState1115 = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + '1115states.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getState340b = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + '340bstates.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getStateTelehealth = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'telehealthstates.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getCategoryTelehealth = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'category.json').subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__telehealth_telehealth__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_rest__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_seo_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_auth_service__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomePage = (function () {
    function HomePage(nav, restProvider, auth, navCtrl, seo) {
        this.restProvider = restProvider;
        this.auth = auth;
        this.navCtrl = navCtrl;
        this.nav = nav;
        this.getStates();
        seo.addTwitterCard('Substance Use Disorder', 'Substance Use Disorder', 'Medicaid Coverage Waivers: State Profiles', '../../assets/images/logo.png');
    }
    HomePage.prototype.ionViewCanEnter = function () {
        return this.auth.accesspage('sud');
    };
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var TIME_IN_MS = 500;
        var hideFooterTimeout = setTimeout(function () {
            USAmapLoad('usaTerritories', 'sud', '1');
            googleTagManager(_this.auth.getUid());
        }, TIME_IN_MS);
    };
    HomePage.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
    };
    HomePage.prototype.goToTelehealthPage = function (pageName) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__telehealth_telehealth__["a" /* TelehealthPage */]);
    };
    HomePage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getState()
            .then(function (data) {
            _this.states = data;
            //console.log(this.states);
        });
    };
    ;
    HomePage.prototype.logout = function () {
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\home\home.html"*/'\n\n\n\n\n\n	<ion-content class="page-home" cache-view="false" view-title="My Title!">\n\n		<my-main-header  pagetitle="Home"></my-main-header>\n\n	<section>\n\n		<div class="banner"> \n\n			<img src="../../assets/images/back.jpg">\n\n			<div class="container">\n\n				<h1>Substance Use Disorder</h1>\n\n			</div>\n\n		</div>\n\n	</section>\n\n\n\n	<!------>\n\n	\n\n	 <ul *ngFor="let item of arr" class="list-group">\n\n    <li class="list-group-item">\n\n     {{item.pages}}\n\n     {{user_uids}}\n\n    </li>\n\n\n\n  </ul>\n\n  \n\n\n\n	<section>\n\n		<div class="container">\n\n			<div class="outer">\n\n				<div class="left">\n\n					<h5>Medicaid Coverage Waivers: State Profiles</h5>\n\n					<a href="#" class="download_att_sud">Dowload attachment</a>\n\n				</div>\n\n				<div class="right">\n\n				<img src="../../assets/images/right-img.png"></div>\n\n			</div>\n\n			<div class="content-medi">\n\n			<div class="container">\n\n				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n\n\n				<p>Why do we use it?\n\n				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n\n\n\n				<p>Why do we use it?\n\n				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n\n			</div>\n\n			</div>\n\n		</div>\n\n\n\n	</section>\n\n\n\n	<section class="state-drop-sec">\n\n		<div class="container">\n\n			<div class="state-inner">\n\n				<h5>Select States</h5>\n\n				<select class="select_state_sud" name="statename" >\n\n				<option [disabled]="state.available == 0" *ngFor="let state of states" value="{{state.name}}" >{{state.name}}</option>\n\n				</select>\n\n\n\n				<div class="icons-sec">\n\n					<ul>\n\n						<li><a href="" class="qus_sud"><i class="fas fa-question"></i></a></li>\n\n						<li><a class="chart_sud" onclick="show_chart(1)"><i class="fas fa-chart-line"></i></a></li>\n\n						<li><a class="map_sud" onclick="show_map(1)"><i class="fas fa-globe"></i></a></li>\n\n					</ul>\n\n				</div>\n\n			</div>\n\n		</div>\n\n	</section>\n\n\n\n\n\n\n\n	<!---section-->\n\n	<section>\n\n\n\n		<div class="container">\n\n			<div class="map_section">\n\n				<div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map1"></div>\n\n			</div>\n\n		</div>\n\n		<div id="state_data"></div>\n\n\n\n\n\n\n\n		<div class="table_hidee">\n\n			<div class="table_hide">\n\n			\n\n				<div>	<section class="Tble" id="vertical-ticker" >\n\n			                <table style="width:100%" id="example">\n\n			                  <thead>\n\n			                     <tr>\n\n			                         <th colspan=""></th>\n\n			                         <th colspan=""></th>\n\n			                         <th colspan="3">Expenditure Authority for Enhanced Benefits for Individual swith SUD</th>\n\n			                         <th colspan="2">Waiver of IMD Exclusion</th>\n\n			                         <th colspan=""></th>\n\n			                         <th colspan=""></th>\n\n			                         <th colspan=""></th>\n\n			                         <th colspan=""></th>\n\n			                     </tr>\n\n			                     <tr>\n\n			                     <th></th>\n\n			                      <th>Waiver Status</th>\n\n			                     <th colspan="">HCBS</th>\n\n			                     <th colspan="">Case Management/ Care Coordination</th>\n\n			                     <th colspan="">Peer Supports</th>\n\n			                     <th colspan="">SUD</th>\n\n			                     <th colspan="">MH</th>\n\n			                     <th>Delivery System Reform WithSUD Component</th>\n\n			                     <th>Coverage Expansion for Individuals With SUD</th>\n\n			                     <th>Specialized Integrated Managed Care Plan Targeted to Population With SUD</th>\n\n			                     <th>Alternative Payment Methodology</th>\n\n\n\n			                     </tr>\n\n			                  </thead>\n\n			                  <tbody>\n\n\n\n			                      <tr>\n\n			                      <td>AK</td>\n\n			                      <td>Pending</td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td><span class=""></span></td>\n\n			                      <td><span></span></td>\n\n			                      <td><span></span></td>\n\n			                      <td><span></span></td>\n\n			                      <td><span></span></td>\n\n\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>AZ</td>\n\n			                      <td>Approved with no SUD features; SUD features pending</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class=""></span></td>\n\n			                      <td><span class=""></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class=""></span></td>\n\n			                      <td></td>\n\n\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>CA</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n\n\n			                    </tr>\n\n\n\n			                      <tr>\n\n			                      <td>DE</td>\n\n			                      <td>Approved with SUD features; additional SUD features pending</td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n\n\n			                    </tr>\n\n\n\n\n\n\n\n			                    <tr>\n\n			                      <td>FL</td>\n\n			                      <td>Approved with no SUD features; SUD features pending</td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n\n\n			                    </tr>\n\n\n\n			                    <tr>\n\n			                      <td>HI</td>\n\n			                      <td>Approved with no SUD features; SUD features pending</td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n\n\n			                    </tr>\n\n\n\n			                    <tr>\n\n			                      <td>IL</td>\n\n			                      <td>Approved</td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n\n\n			                    </tr>\n\n\n\n			                      <tr>\n\n			                      <td>IN</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>KS</td>\n\n			                      <td>Approved with SUD features; additional SUD features pending</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr red"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>KY</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                      <tr>\n\n			                      <td>LA</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                      <tr>\n\n			                      <td>MA</td>\n\n			                      <td>Approved</td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                      <tr>\n\n			                      <td>MD</td>\n\n			                      <td>Approved with SUD features; additional SUD features pending</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                      <tr>\n\n			                      <td>MI</td>\n\n			                      <td>Pending</td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>MN</td>\n\n			                      <td>Pending</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>MO</td>\n\n			                      <td>Approved with no SUD features; SUD features pending</td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                      <tr>\n\n			                      <td>NC</td>\n\n			                      <td>Pending</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n\n\n			                     <tr>\n\n			                      <td>NH4</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                    </tr>\n\n\n\n			                    <tr>\n\n			                      <td>NU</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>NM</td>\n\n			                      <td>Approved with no SUD features; SUD features pending</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>NY</td>\n\n			                      <td>Approved with SUD features; additional SUD features pending</td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                    <tr>\n\n			                      <td>PA</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n\n\n			                    <tr>\n\n			                      <td>RI</td>\n\n			                      <td>Approved with SUD features; additional SUD features pending</td>\n\n			                      <td></td>\n\n			                      <td><span class="cr red"></span></td>\n\n			                      <td><span class="cr red"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>TN</td>\n\n			                      <td>Pending</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                    <tr>\n\n			                      <td>UT</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                    <tr>\n\n			                      <td>VA</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>VT</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>WA</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n\n\n\n\n			                      <tr>\n\n			                      <td>WI</td>\n\n			                      <td>Approved with no SUD features; SUD features pending</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr green"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n\n\n			                     <tr>\n\n			                      <td>WV</td>\n\n			                      <td>Approved</td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td><span class="cr blue"></span></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                      <td></td>\n\n			                    </tr>\n\n			                  </tbody>\n\n			                  </table>\n\n			      </section>\n\n\n\n</div>\n\n				<div class="guide">\n\n					<ul>\n\n						<li><span><img src="../../blue.png"></span><h5>= Waiver obtained </h5></li>\n\n						<li><span><img src="../../green.png"></span><h5> = Waiver requested</h5></li>\n\n						<li><span><img src="../../red.png"></span><h5>= No waiver required</h5></li>\n\n					</ul>\n\n				</div>\n\n				<div class="table-controls">\n\n\n\n					<button id="download-xlsx">Download XLSX</button>\n\n\n\n				</div>\n\n			</div>\n\n		</div>\n\n\n\n	</section>\n\n	<!--section-->\n\n\n\n\n\n\n\n	<!----footer-->\n\n    <footer>\n\n		<div class="container">\n\n			<div class="foot-outr">\n\n				<div class="left-foot">\n\n					<img src="">\n\n				</div>\n\n				<div class="right-foot">\n\n					<ul>\n\n						<li><a href="">Privacy</a></li>\n\n						<li><a href="/#/340b">340B</a></li>\n\n						<li><a href="/#/1115states" >1115 States</a></li>\n\n					</ul>\n\n				</div>\n\n			</div>\n\n			<div class="lower-foot">\n\n				<ul>\n\n					<li>Follow us:</li>\n\n					<li>\n\n						<a href=""><i class="fab fa-facebook"></i> </a>\n\n						<a href=""><i class="fab fa-twitter"></i></a>\n\n						<a href=""><i class="fab fa-linkedin-in"></i></a>\n\n					</li>\n\n				</ul>\n\n			</div>\n\n		</div>\n\n	</footer>\n\n	<!----footer-->\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\home\home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */],
            __WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_6__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5__services_seo_service__["a" /* SeoService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.page.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TelehealthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_rest__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_seo_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TelehealthPage = (function () {
    function TelehealthPage(nav, navCtrl, restProvider, http, auth, seo) {
        this.nav = nav;
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.http = http;
        this.auth = auth;
        seo.addTwitterCard('Telehealth', 'Telehealth', 'State Telehealth Laws and Medicaid Policies 50-State Survey Findings', '../../assets/images/logo.png');
        this.getStates();
        this.getCategory();
    }
    TelehealthPage.prototype.ionViewCanEnter = function () {
        return this.auth.accesspage('telehealth');
    };
    TelehealthPage.prototype.ionViewDidLoad = function () {
        var TIME_IN_MS = 500;
        var hideFooterTimeout = setTimeout(function () {
            USAmapLoad('usaTerritories1', 'telehealth', '2');
        }, TIME_IN_MS);
    };
    TelehealthPage.prototype.getStates = function () {
        var _this = this;
        this.restProvider.getStateTelehealth()
            .then(function (data) {
            _this.states = data;
        });
    };
    ;
    TelehealthPage.prototype.getCategory = function () {
        var _this = this;
        this.restProvider.getCategoryTelehealth()
            .then(function (data) {
            _this.category = data;
        });
    };
    ;
    TelehealthPage.prototype.onSelectChange = function (selectedValue) {
        stateChange(selectedValue);
    };
    TelehealthPage.prototype.onCategoryChange = function (selectedValue) {
        categoryChange(selectedValue);
    };
    TelehealthPage.prototype.logout = function () {
        this.auth.signOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    TelehealthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-telehealth',template:/*ion-inline-start:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\telehealth\telehealth.html"*/'\n<ion-content class="page-home" cache-view="false" view-title="My Title!">\n	<my-main-header pagetitle="Telehealth"></my-main-header>\n	<!------>\n	<section>\n		<div class="banner">\n			<img src="../../assets/images/team.jpg">\n			<div class="container">\n				<h1>Telehealth</h1>\n			</div>\n		</div>\n	</section>\n\n	<!------>\n\n	<section>\n		<div class="container">\n			<div class="outer">\n				<div class="left">\n					<h5>State Telehealth Laws and Medicaid Policies: <br> 50-State Survey Findings</h5>\n					<a href="#" class="download_att_telehealth">Dowload attachment</a>\n				</div>\n				<div class="right">\n				<img src="../../assets/images/right-img.png"></div>\n			</div>\n\n		</div>\n\n	</section>\n\n	<section>\n	<div class="content-medi">\n				<div class="container">\n\n\n					<div class="desprective-content">\n						<ul class="research-img">\n							<li>\n								<a href="#">\n									<img src="../../assets/images/Jacqueline.jpg">\n								</a>\n								<div class="research_info">\n									<h6>Jacqueline D. Marks</h6>\n									<p>MANAGER</p>\n								</div>\n							</li>\n							<li>\n								<a href="#">\n									<img src="../../assets/images/Augenstein_Jared.jpg">\n								</a>\n								<div class="research_info">\n									<h6>Jared Augenstein</h6>\n									<p> SR MANAGER</p>\n								</div>\n							</li>\n							<li>\n								<a href="#">\n									<img src="../../assets/images/Seigel_Randi.jpg">\n								</a>\n								<div class="research_info">\n									<h6>Randi Seigel</h6>\n									<p>Partner</p>\n								</div>\n							</li>\n						</ul>\n						<p>The use of telemedicine services is growing nationwide as providers and payors seek to improve access and better manage patient care, while reducing health care spending. State laws and Medicaid<sup><a style="color:undefined" href="javascript:void(0)" class="state_check" data-tab="sourcepara">[1]</a></sup> policies related to reimbursement, licensure and practice standards are rapidly evolving in response to the proliferation of technology and the growing evidence base that demonstrates the impact of telemedicine on access, quality and cost of care. Some states have been proactive in encouraging the use of telemedicine as a means to enhance services in rural areas, increase access to care for members with complex conditions, and reduce costs associated with unnecessary emergency department visits.</p>\n						<p>In light of this rapidly changing landscape, Manatt Health has conducted a 50-state survey of state laws and Medicaid program policies related to telemedicine in the following key areas:</p>\n						<ul class="prod_list">\n							<li>Practice standards and licensure</li>\n							<li>Coverage and reimbursement</li>\n							<li>Eligible patient settings</li>\n							<li>Eligible provider types</li>\n							<li>Eligible technologies, and</li>\n							<li>Service limitations.</li>\n						</ul>\n						<p>Based on survey results, we classified state telemedicine policies as\n						"progressive," "moderate," or "restrictive" across each of these categories, as captured in the state map below.</p>\n						<p>Our analysis suggests that telemedicine will be critical to delivering health care in the future, and state Medicaid policies are evolving - in some states more quickly than others - to accelerate adoption of telemedicine models. As technology advances and the evidence base for telemedicine expands, state policy will continue to evolve to integrate telemedicine into payment and delivery reforms that support overarching program objectives related to access, quality, and cost of care.</p>\n						<p>Additional findings from our analysis can be found here.</p>\n						<p>To access a detailed summary profile of a state\'s telehealth laws and Medicaid policies, click the state on the map below. To explore state classifications across survey categories, use the State Classification Category drop down menu or hover your mouse over a state of interest. Explore further by using the controls at the top left of the map that allow you to zoom in/out, pan or type a state name in the search bar to quickly access a state of interest.</p>\n					</div>\n				</div>\n\n			</div>\n\n	</section>\n	<!---section-->\n	<section class="state-drop-sec">\n		<div class="container">\n			<div class="state-inner">\n\n				<div class="select_dropdown teleheath_section">\n				<div class="col_50">\n					<h5>Select States</h5>\n					<select class="select_state_telehealth"  name="statename" >\n						<option *ngFor="let state of states" value="{{state.abbreviation}}" >{{state.name}}</option>\n					</select>\n				</div>\n				<div class="col_50">\n\n					<h5>Select Categories</h5>\n					<select class="select_cat_telehealth" name="catname"  >\n						<option *ngFor="let cat of category" value="{{cat.title}}" >{{cat.title}}</option>\n					</select>\n				</div>\n				</div>\n				<div class="icons-sec">\n					<ul>\n						<li><a href="" class="qus_telehealth" ><i class="fas fa-question"></i></a></li>\n						<li><a class="map_telehealth" class="show_map" onclick="show_map(2)"><i class="fas fa-globe"></i></a></li>\n					</ul>\n				</div>\n			</div>\n		</div>\n	</section>\n\n	<section>\n		<div class="map_section">\n			<div data-step="2" data-intro="Click on state" class="jsmaps-wrapper" id="usaTerritories-map2"></div>\n		</div>\n\n		<div id="state_data"></div>\n\n\n	</section>\n	<!--section-->\n\n\n	<!----footer-->\n    <footer>\n		<div class="container">\n			<div class="foot-outr">\n				<div class="left-foot">\n					<img src="">\n				</div>\n				<div class="right-foot">\n					<ul>\n						<li><a href="">Privacy</a></li>\n						<li><a href="/#/340b">340B</a></li>\n						<li><a href="/#/1115states" >1115 States</a></li>\n					</ul>\n				</div>\n			</div>\n			<div class="lower-foot">\n				<ul>\n					<li>Follow us:</li>\n					<li>\n						<a href=""><i class="fab fa-facebook"></i> </a>\n						<a href=""><i class="fab fa-twitter"></i></a>\n						<a href=""><i class="fab fa-linkedin-in"></i></a>\n					</li>\n				</ul>\n			</div>\n		</div>\n	</footer>\n	<!----footer-->\n\n</ion-content>\n'/*ion-inline-end:"E:\Alex projects\ionicproject\mannat-survey-master\mannat-survey-master\src\pages\telehealth\telehealth.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Nav */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__services_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_6__services_seo_service__["a" /* SeoService */]])
    ], TelehealthPage);
    return TelehealthPage;
}());

//# sourceMappingURL=telehealth.js.map

/***/ })

},[314]);
//# sourceMappingURL=main.js.map